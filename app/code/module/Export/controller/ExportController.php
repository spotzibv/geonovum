<?php
/**
 * Class ExportController - Export controller.
 *
 * @category    Geonovum
 * @package     Module
 * @subpackage  Export
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class ExportController extends ModuleController {
        public function data() {
                $this->render();
        }
}