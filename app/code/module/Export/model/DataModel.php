<?php
/**
 * Class DataModel - Export visualization model.
 *
 * @category    Geonovum
 * @package     Module
 * @subpackage  Export
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class DataModel extends ModuleModel {
        // User variable
        public $user                            = array();
        // Visualization variable
        public $visualization                   = array();

        // General
        public $query                           = null;
        public $layerId                         = null;
        public $limit                           = VISUALIZATION_CONTENT_LIMIT_DEFAULT;

        public function validateRequestParams() {
                $loggedIn = Session::getData(REQUEST_PARAMETER_LOGGEDIN);
                $this->user = Session::getData(REQUEST_PARAMETER_USER_NAME);

                $this->visualization  = $this->getVisualization();
                if (!isset($this->visualization[REQUEST_PARAMETER_VIZ_ID]) || !$this->visualization[REQUEST_PARAMETER_VIZ_ID])
                        ErrorHandler::error(E_ERROR, 'An invalid visualization was requested');

                $this->layerId = $this->getParam(REQUEST_PARAMETER_VIZ_LAYER_ID);
                if (!$this->layerId) $this->layerId = $this->visualization['data_layer'];

                if (!$loggedIn && $this->layerId === $this->visualization['proposition_layer'])
                        ErrorHandler::error(E_ERROR, 'This action is not allowed');

                $this->query = $this->getParam(REQUEST_PARAMETER_QUERY);
                if (!$this->query) ErrorHandler::error(E_ERROR, "Missing parameter '%s'", REQUEST_PARAMETER_QUERY);

                $limit = $this->getParam(REQUEST_PARAMETER_CONTENT_LIMIT);
                if (is_numeric($limit) && $limit > 0 && $limit < $this->limit)
                        $this->limit = $limit;

//                require_once(Text::prepare(DIR_APP_MODULE_SPECIFIC, 'Location') . DIR_MODEL . 'SearchModel.php');
        }

        public function export() {
                $result = array();
                if ($this->query) {
                        $vizUser = $this->getVisualizationUser($this->visualization[REQUEST_PARAMETER_USER_NAME]);
                        $apiKey = (isset($vizUser['api_key']) ? $vizUser['api_key'] : false);

                        if ($apiKey) {
                                $vizJSON = $this->getVisualizationJSON();
                                $table = null;
                                if (isset($vizJSON['layers'])) {
                                        // @todo: layer ID parameter
                                        foreach ($vizJSON['layers'] as $layer) {
                                                if (isset($layer['type']) && $layer['type'] === 'layergroup') {
                                                        foreach ($layer['options']['layer_definition']['layers'] as $mapLayer) {
                                                                if ($mapLayer['id'] === $this->layerId) {
                                                                        $queryArray = explode(' ', str_replace("\n", ' ', strtolower($mapLayer['options']['sql'])));
                                                                        $indexFROM = array_search('from', $queryArray);
                                                                        if ($indexFROM && array_key_exists($indexFROM + 1, $queryArray)) {
                                                                                $table = $queryArray[$indexFROM + 1];
                                                                        } else {
                                                                                ErrorHandler::error(E_ERROR, 'Could not extract the table name from layer');
                                                                        }
                                                                        break 2;
                                                                }
                                                        }
                                                }
                                        }
                                }

                                if ($table) {
                                        $query = "SELECT *, ST_X(ST_Centroid(the_geom)) AS centerx, ST_Y(ST_Centroid(the_geom)) AS centery FROM {$table} AS t WHERE t::text ILIKE '%{$this->query}%'";

                                        if ($this->layerId === $this->visualization['proposition_layer']) {
                                                if ($this->visualization['edit_proposition_private'])
                                                        $query .= " AND t.user_name = '{$this->user['Name']}'";

                                                $query .= ' ORDER BY t.created_at DESC';
                                        }

                                        $query .= " LIMIT {$this->limit};";

                                        $webserviceUrl = WEBSERVICE_URL . 'visualization/wo/sql';
                                        $webserviceParams = array('userName'    => $this->visualization[REQUEST_PARAMETER_USER_NAME],
                                                                  'userKey'     => $apiKey,
                                                                  'q'           => $query,
                                                                  'format'      => 'application/json');

                                        $webserviceUrl .= '?' . http_build_query($webserviceParams);

                                        $result = array();
                                        $webserviceResult = Connectivity::runCurl($webserviceUrl);
                                        if ($webserviceResult) {
                                                $webserviceContents = json_decode($webserviceResult, true);

                                                if (isset($webserviceContents['response']['sql']))
                                                        $result = $webserviceContents['response']['sql'];
                                        }
                                }
                        }
                }

                return array(REQUEST_RESULT     => $result);
        }
}