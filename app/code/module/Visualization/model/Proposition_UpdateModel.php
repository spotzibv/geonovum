<?php
/**
 * Class PropositionUpdateModel - Visualization proposition_update model.
 *
 * @category    Geonovum
 * @package     Module
 * @subpackage  Visualization
 * @author      Erik Nagelkerke <erik@spotzi.com>
 */
class Proposition_UpdateModel extends ModuleModel {
        // User variable
        public $user                            = array();
        // Visualization variable
        public $visualization                   = array();
        // Proposition ID
        public $propositionId                   = null;
        // New Status
        public $newStatus                       = null;

        public function validateRequestParams() {
                $loggedIn = Session::getData(REQUEST_PARAMETER_LOGGEDIN);
                $this->user = Session::getData(REQUEST_PARAMETER_USER_NAME);
                if (!$loggedIn || !isset($this->user['Name'], $this->user['Email']))
                        ErrorHandler::error(E_ERROR, 'This action is not allowed');

                $this->visualization = $this->getVisualization();
                if (!isset($this->visualization[REQUEST_PARAMETER_VIZ_ID]) || !$this->visualization[REQUEST_PARAMETER_VIZ_ID])
                        ErrorHandler::error(E_ERROR, 'An invalid visualization was requested');

                if (!$this->visualization[REQUEST_PARAMETER_MYMAP])
                        ErrorHandler::error(E_ERROR, 'Only My Maps are allowed');

                if (!$this->visualization['proposition_layer'])
                        ErrorHandler::error(E_ERROR, 'No proposition layer identifier in visualization');

                if (!$this->visualization['data_layer'])
                        ErrorHandler::error(E_ERROR, 'No data_layer identifier in visualization');

                $this->propositionId = $this->getParam('propositionId');
                $this->newStatus = $this->getParam('newStatus');
                $this->reviewDescription = $this->getParam('reviewDescription');
                if (!$this->propositionId || !$this->newStatus)
                        ErrorHandler::error(E_ERROR, Text::prepare('Missing request data, expected propositionId and newStatus, got %s and %s', $this->propositionId, $this->newStatus));
        }

        public function handlePropositionUpdate() {
                // Get proposition columns
                $vizJSON = $this->getVisualizationJSON();
                $table = null;
                if (isset($vizJSON['layers'])) {
                        foreach ($vizJSON['layers'] as $layer) {
                                if (isset($layer['type']) && $layer['type'] === 'layergroup') {
                                        foreach ($layer['options']['layer_definition']['layers'] as $mapLayer) {
                                                if ($mapLayer['id'] === $this->visualization['proposition_layer']) {
                                                        $queryArray = explode(' ', str_replace("\n", ' ', strtolower($mapLayer['options']['sql'])));
                                                        $indexFROM = array_search('from', $queryArray);
                                                        if ($indexFROM && array_key_exists($indexFROM + 1, $queryArray)) {
                                                                $table = $queryArray[$indexFROM + 1];
                                                        } else {
                                                                ErrorHandler::error(E_ERROR, 'Could not extract the table name from proposition layer');
                                                        }
                                                        break 2;
                                                }
                                        }
                                }
                        }
                }

                $proposition = array();
                if ($table) {
                        $queryProposition = Text::prepare('SELECT *, ST_AsGeoJSON(the_geom) as the_geom FROM %s WHERE cartodb_id = %s LIMIT 1;', $table, $this->propositionId);
                        $queryPropositionResult = Connectivity::runCurl(Text::prepare('http://%s.spotzi.me/api/v2/sql', $this->user['Name']),
                                                                        array(CURLOPT_POST              => 1,
                                                                              CURLOPT_POSTFIELDS        => array('q'            => $queryProposition,
                                                                                                                 'api_key'      => $this->user['ApiKey'])));
                        if ($queryPropositionResult) {
                                $propositionJSON = json_decode($queryPropositionResult, true);

                                if (isset($propositionJSON['rows'][0])) $proposition = $propositionJSON['rows'][0];
                        } else {
                                ErrorHandler::error(E_ERROR, 'Could not find the proposition in the proposition layer');
                        }
                } else {
                        ErrorHandler::error(E_ERROR, 'Got bad result while getting the proposition info');
                }

                $result = true;
                if ($proposition) {
                        if ($this->newStatus === 'accepted') {
                                // Handle proposition edits
                                $propositionPerform = [
                                        'visualizationId'       => $this->visualization[REQUEST_PARAMETER_VIZ_ID],
                                        'actions'               => $proposition['feature_action'],
                                        'layerId'               => $this->visualization['data_layer'],
                                        'featureId'             => $proposition['feature_id'],
                                        'the_geom'              => $proposition['the_geom'],
                                        'columnData'            => $proposition['column_data'],
                                        'style'                 => $proposition['feature_style']
                                ];

                                $webserviceUrl = WEBSERVICE_URL . 'visualization/wo/edit_layer';
                                $webserviceParams = array('user'        => WEBSERVICE_USER,
                                                          'password'    => WEBSERVICE_PASSWORD,
                                                          'userName'    => $this->user['Name'],
                                                          'userKey'     => $this->user['ApiKey'],
                                                          'editInfo'    => json_encode($propositionPerform),
                                                          'format'      => 'application/json');
                                $webserviceResult = Connectivity::runCurl($webserviceUrl,
                                                                          array(CURLOPT_CUSTOMREQUEST   => 'POST',
                                                                                CURLOPT_POSTFIELDS      => $webserviceParams));

                                if (!$webserviceResult)
                                        ErrorHandler::error(E_ERROR, 'Something went wrong while handling the proposition');
                        }

                        // Set proposition to newStatus
                        $propositionUpdate = [
                                'visualizationId'       => $this->visualization[REQUEST_PARAMETER_VIZ_ID],
                                'actions'               => 'EDIT_DATA',
                                'layerId'               => $this->visualization['proposition_layer'],
                                'featureId'             => $this->propositionId,
                                'columnData'            => ['status' => $this->newStatus, 'review_description' => $this->reviewDescription]
                        ];

                        $webserviceUrl = WEBSERVICE_URL . 'visualization/wo/edit_layer';
                        $webserviceParams = array('user'        => WEBSERVICE_USER,
                                                  'password'    => WEBSERVICE_PASSWORD,
                                                  'userName'    => $this->user['Name'],
                                                  'userKey'     => $this->user['ApiKey'],
                                                  'editInfo'    => json_encode($propositionUpdate, true),
                                                  'format'      => 'application/json');

                        $propositionUpdateResult = Connectivity::runCurl($webserviceUrl,
                                                                         array(CURLOPT_CUSTOMREQUEST    => 'POST',
                                                                               CURLOPT_POSTFIELDS       => $webserviceParams));

                        if ($propositionUpdateResult) {
                                $propositionUpdateContents = json_decode($propositionUpdateResult, true);

                                if (isset($propositionUpdateContents['response']['edit_layer'])) {
                                        $updateResult = $propositionUpdateContents['response']['edit_layer'];
                                        if (isset($updateResult['success']) && $updateResult['success'])
                                                $result = $updateResult['success'];
                                }
                        }

                        if ($result && $proposition['user_name'] !== $this->user['Name']) {
                                $vizUser = $this->getVisualizationUser($proposition['user_name']);
                                $email = (isset($vizUser['email']) ? $vizUser['email'] : false);

                                if ($email) {
                                        $actions = explode(',', $proposition['feature_action']);

                                        $actionTypes = array();
                                        foreach ($actions as $action) {
                                                switch ($action) {
                                                        case EDITOR_ACTION_NEW_FEATURE:
                                                                $actionTypes[] = __('New feature');
                                                                break;
                                                        case EDITOR_ACTION_EDIT_DATA:
                                                                $actionTypes[] = __('Data edit');
                                                                break;
                                                        case EDITOR_ACTION_EDIT_GEOM:
                                                                $actionTypes[] = __('Geometry edit');
                                                                break;
                                                        case EDITOR_ACTION_EDIT_STYLE:
                                                                $actionTypes[] = __('Style edit');
                                                                break;
                                                        case EDITOR_ACTION_DELETE:
                                                                $actionTypes[] = __('Feature delete');
                                                                break;
                                                }
                                        }
                                        $actionType = implode(' / ', $actionTypes);

                                        $updateId = $this->visualization[REQUEST_PARAMETER_VIZ_ID] . '-' . $this->propositionId;
                                        $updateUrl = Url::buildPlatformUrl(false, null, null, null, null,
                                                                           array(REQUEST_PARAMETER_UPDATE_ID    => $updateId));

                                        $status = '';
                                        switch ($this->newStatus) {
                                                case 'accepted':
                                                        $status = __('Accepted');
                                                        $propositionName = $proposition['name'];
                                                        $propositionDescription = $proposition['description'];
                                                        $propositionStatus = 'accepted';
                                                        $propositionUser = $proposition['user_name'];
                                                        if ($this->visualization[REQUEST_PARAMETER_VIZ_ID] === 'e18380e6-e5d9-11e5-b7f2-00265522ea30') {
                                                                // Set proposition to newStatus
                                                                $treesUpdate = [
                                                                        'visualizationId'       => 'c9a1aa50-e5f4-11e5-b7f2-00265522ea30',
                                                                        'actions'               => 'NEW_FEATURE',
                                                                        'layerId'               => '39b04207-9ef9-4fa2-9442-3b8fc73edb0d',
                                                                        'featureId'             => '',
                                                                        'columnData'            => [    'category' => 'boom',
                                                                                                        'descriptio' => $this->reviewDescription,
                                                                                                        'datum' => date('Y-m-d'),
                                                                                                        'collectie' => 'officiële publicatie',
                                                                                                        'city' => 'HALSTEREN',
                                                                                                        'collectienaam' => 'bomen',
                                                                                                        'dtd' => 'Officiële Publicaties, versie 1.1',
                                                                                                        'identifier' => 'wsb-2016-825',
                                                                                                        'overheidop_referentienummer' => 'WBD15-01186',
                                                                                                        'url' => 'http://demo.spotzi.com/geonovum/gemeente.html'],
                                                                        'the_geom'              => $proposition['the_geom']
                                                                ];

                                                                $webserviceUrl = WEBSERVICE_URL . 'visualization/wo/edit_layer';
                                                                $webserviceParams = array('user'        => WEBSERVICE_USER,
                                                                                          'password'    => WEBSERVICE_PASSWORD,
                                                                                          'userName'    => $this->user['Name'],
                                                                                          'userKey'     => $this->user['ApiKey'],
                                                                                          'editInfo'    => json_encode($treesUpdate, true),
                                                                                          'format'      => 'application/json');

                                                                $treesUpdateResult = Connectivity::runCurl($webserviceUrl,
                                                                                                                 array(CURLOPT_CUSTOMREQUEST    => 'POST',
                                                                                                                       CURLOPT_POSTFIELDS       => $webserviceParams));


                                                                // Write update to http://demo.spotzi.com/geonovum/gemeente.html
                                                                $gemeenteFile = '\\\\192.168.10.19\\wwwroot\\demo.spotzi.com\\geonovum\\gemeente.html';
                                                                //$gemeenteFile = 'C:\\inetpub\wwwroot\demo.spotzi.com\geonovum\gemeente.html';
                                                                $content = file_get_contents($gemeenteFile);
                                                                while ($content === '') {
                                                                        $content = file_get_contents($gemeenteFile);
                                                                }

                                                                $authorityName = 'Gemeente Bergen op Zoom';
                                                                $propositionComment = $this->reviewDescription;
                                                                ob_start();
                                                                require_once($this->modulePath . DIR_VIEW . 'Bekendmakingen/bekendmaking.php');
                                                                $new_row = ob_get_clean();
                                                                $new_content = str_replace("<!--new_rows-->", $new_row, $content);
                                                                if ($new_content !== '')
                                                                        file_put_contents($gemeenteFile, $new_content);
                                                        }


                                                        if ($proposition['feature_id']) {
                                                                $queryPropositionUsers = Text::prepare("SELECT ('''' || string_agg(DISTINCT user_name, ''',''') || '''') as users FROM %s WHERE feature_id = %s AND NOT user_name IN ('%s','%s');", $table, $proposition['feature_id'], $proposition['user_name'], $this->user['Name']);
                                                                $sqlApiUrl = Text::prepare('http://%s.spotzi.me/api/v2/sql', $this->user['Name']);
                                                                $propositionUsersResult = Connectivity::runCurl($sqlApiUrl,
                                                                                                    array(CURLOPT_CUSTOMREQUEST    => 'POST',
                                                                                                          CURLOPT_POSTFIELDS       => ['q' => $queryPropositionUsers]));

                                                                if ($propositionUsersResult) {
                                                                        $propositionUsersJSON = json_decode($propositionUsersResult, true);
                                                                        if (isset($propositionUsersJSON['rows']) && $propositionUsersJSON['rows']) {
                                                                                $mailingListUsers = $propositionUsersJSON['rows'][0]['users'];
                                                                                if(!is_null($mailingListUsers)) {
                                                                                        $queryEmails = Text::prepare("SELECT email FROM users WHERE username IN (%s)", $mailingListUsers);
                                                                                        $emailResult = PostgreSqlDatabase::getInstance()->execute(self::DB_CONNECTION_CARTO_READ, $queryEmails);
                                                                                        if ($emailResult) {
                                                                                                ob_start();
                                                                                                require_once($this->modulePath . DIR_VIEW . 'mail/update_proposition_concurrent.php');
                                                                                                $messageConcurrent = ob_get_clean();
                                                                                                // Prepare the updated user mailer
                                                                                                Mail::addMailer(EMAIL_HOST, EMAIL_PORT, EMAIL_FROM, EMAIL_FROM_PASSWORD, BRAND_PRODUCT);
                                                                                                foreach ($emailResult as $rowEmail) {
                                                                                                        // Send the updated user email
                                                                                                        Mail::send($rowEmail['email'], EMAIL_FROM, BRAND_PRODUCT . __(' - there was an update on the map you have proposed on'),
                                                                                                                   $messageConcurrent, array(), array(), array(), true, true);
                                                                                                }
                                                                                        }
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                        break;
                                                case 'declined':
                                                        $status = __('Declined');
                                                        break;
                                                case 'pending':
                                                        $status = __('Pending');
                                                        break;
                                        }

                                        // Retrieve the updated user email template
                                        ob_start();
                                        require_once($this->modulePath . DIR_VIEW . 'mail/updated_user.php');
                                        $message = ob_get_clean();

                                        // Prepare the updated user mailer
                                        Mail::addMailer(EMAIL_HOST, EMAIL_PORT, EMAIL_FROM, EMAIL_FROM_PASSWORD, BRAND_PRODUCT);
                                        // Send the updated user email
                                        Mail::send($email, EMAIL_FROM, BRAND_PRODUCT . __(' - your map update was reviewed'),
                                                   $message, array(), array(), array(), true, true);
                                }
                        }
                }

                return array(REQUEST_RESULT     => $result);
        }
}