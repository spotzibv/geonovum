<?php
/**
 * Class AddFeatureModel - Visualization add feature model.
 *
 * @category    Geonovum
 * @package     Module
 * @subpackage  Visualization
 * @author      Erik Nagelkerke <erik@spotzi.com>
 */
class AddFeatureModel extends ModuleModel {
        // General
        const EDITOR_POINT                      = 'POINT';
        const EDITOR_LINE                       = 'MULTILINESTRING';
        const EDITOR_LINE_SHORT                 = 'LINE';
        const EDITOR_POLYGON                    = 'MULTIPOLYGON';
        const EDITOR_POLYGON_SHORT              = 'POLYGON';

        // User variable
        public $user                            = array();
        // Visualization variable
        public $visualization                   = array();

        public $action                          = null;
        public $featureId                       = null;
        public $layerId                         = null;
        public $the_geom                        = null;
        public $featureStyle                    = null;

        public function validateRequestParams() {
                $loggedIn = Session::getData(REQUEST_PARAMETER_LOGGEDIN);
                $this->user = Session::getData(REQUEST_PARAMETER_USER_NAME);
                if (!$loggedIn || !isset($this->user['Name'], $this->user['Email']))
                        ErrorHandler::error(E_ERROR, 'This action is not allowed');

                $this->visualization  = $this->getVisualization();
                if (!isset($this->visualization[REQUEST_PARAMETER_VIZ_ID]) || !$this->visualization[REQUEST_PARAMETER_VIZ_ID] ||
                    (!$this->visualization[REQUEST_PARAMETER_MYMAP] && (!$this->visualization['map_enabled'] || !$this->visualization['edit_enabled'])))
                        ErrorHandler::error(E_ERROR, 'An invalid visualization was requested');

                $this->action = $this->getParam('featureAction');
                $this->featureId = ($this->getParam('featureId') ? (int) $this->getParam('featureId') : null);
                $this->layerId = $this->visualization['data_layer'];
                $this->the_geom = ($this->getParam('the_geom') ? $this->getParam('the_geom') : null);
                $this->featureStyle = $this->getParam('featureStyle');
                $this->featureStyleChanged = (boolean)($this->getParam('featureStyleChanged') === 'true');

                switch ($this->action) {
                        case EDITOR_ACTION_NEW_FEATURE:
                                if (!$this->the_geom || is_null($this->featureStyle) || !$this->getParam('name') || strcmp($this->getParam('name'), '') === 0)
                                        ErrorHandler::error(E_ERROR, 'Feature data missing: expected the_geom, featureStyle and name');
                                break;
                        case EDITOR_ACTION_EDIT_DATA:
                                if (!$this->featureId || !$this->layerId || is_null($this->featureStyle) || !$this->getParam('name') || strcmp($this->getParam('name'), '') === 0)
                                        ErrorHandler::error(E_ERROR, 'Feature data missing: expected featureId, featureStyle, name and an active data layer');
                                if ($this->featureStyleChanged) // Edit style not needed when only data changed
                                        $this->action = Text::prepare('%s,%s',EDITOR_ACTION_EDIT_DATA, EDITOR_ACTION_EDIT_STYLE);
                                break;
                        case EDITOR_ACTION_EDIT_GEOM:
                                if (!$this->featureId || !$this->layerId || !$this->the_geom)
                                        ErrorHandler::error(E_ERROR, 'Feature data missing: expected featureId, the_geom and an active data layer');
                                break;
                        case EDITOR_ACTION_DELETE:
                                if (!$this->featureId || !$this->layerId)
                                        ErrorHandler::error(E_ERROR, 'Feature data missing: expected featureId and an active data layer');
                                break;
                        default:
                                ErrorHandler::error(E_ERROR, "Action '%s' not permitted", $this->action);
                }
        }

        public function addFeature() {
                // Variables
                $editProposition = $this->visualization['edit_proposition'];

                $featureName = $this->getParam('name');
                $featureDescription = $this->getParam('description');
                $propositionDescription = $this->getParam('proposition_description');

                $allowedFiles = explode(',', str_replace('.', '', UPLOAD_FILES_IMAGE));
                $fileName = File::handleUpload(DIR_TEMP, 'imageurl', null, $allowedFiles, 8388608);     // 8MB
                $imageurl = '';
                if ($fileName) {
                        if (exif_imagetype(DIR_TEMP . $fileName)) {
                                $fileNameNew = Date::format('now', 'YmdHis') . '_' . str_replace(' ', '_', $fileName);

                                $destinationDir = '\\\\db-images\images.spotzi.com\mapbuilder\\users\\' . $this->user['Name'];
                                if (!is_dir($destinationDir)) mkdir($destinationDir);
                                $destination = $destinationDir . '\\' . $fileNameNew;

                                if (is_dir($destinationDir) && copy(DIR_TEMP . $fileName, $destination))
                                        $imageurl = 'http://images.spotzi.com/mapbuilder/users/' . $this->user['Name'] . '/' . $fileNameNew;
                                File::delete(DIR_TEMP . $fileName);
                        } else {
                                File::delete(DIR_TEMP . $fileName);
                                ErrorHandler::error(E_ERROR, '%s is not an image', $fileName);
                        }
                } else {
                        $imageurl = $this->getParam('image');
                }

                // Save to proposition layer
                $vizUser = $this->getVisualizationUser($this->visualization[REQUEST_PARAMETER_USER_NAME]);
                $apiKey = (isset($vizUser['api_key']) ? $vizUser['api_key'] : false);

                $result = false;
                $propositionId = false;
                $dataLayer = end($this->vizJSON['layers']);
                $table = '';
                if ($apiKey && isset($dataLayer['type']) && $dataLayer['type'] === 'layergroup') {
                        $layergroupOptions = $dataLayer['options'];

                        $subLayerStart = (count($layergroupOptions['layer_definition']['layers']) - 1);
                        for ($i = $subLayerStart; $i >= 0; $i--) {
                                $subLayer = $layergroupOptions['layer_definition']['layers'][$i];
                                if ($subLayer['id'] === $this->visualization['proposition_layer']) {
                                        $table = substr($subLayer['options']['sql'], (stripos($subLayer['options']['sql'], ' from ') + 6));
                                        if (strpos($table, ' ') !== false) $table = substr($table, 0, strpos($table, ' '));
                                        break;
                                }
                        }

                        if ($table) {
                                $sqlApiUrl = $layergroupOptions['sql_api_protocol'] . '://' . $layergroupOptions['user_name'] . '.' .
                                             $layergroupOptions['sql_api_domain'] . ':' . $layergroupOptions['sql_api_port'] . $layergroupOptions['sql_api_endpoint'];

                                $columnData = array();
                                if ($featureName) $columnData['name'] = $featureName;
                                if ($featureDescription) $columnData['description'] = $featureDescription;
                                if ($imageurl) $columnData['imageurl'] = $imageurl;

                                // @todo: the_geom_old, old feature style and old column data
                                $data = array('column_data'     => ($columnData ? json_encode($columnData) : null),
                                              'description'     => ($propositionDescription ? $propositionDescription : null),
                                              'feature_action'  => $this->action,
                                              'feature_id'      => ($this->featureId ? $this->featureId : null),
                                              'feature_style'   => ($this->featureStyle ? $this->featureStyle : null),
                                              'name'            => ($featureName ? $featureName : null),
                                              'status'          => ($this->visualization[REQUEST_PARAMETER_MYMAP] ? 'accepted' : 'pending'),
                                              'user_name'       => $this->user['Name']);

                                $connection = PostgreSqlDatabase::getInstance()->getConnection(self::DB_CONNECTION_CARTO_READ);
                                array_walk($data, function(&$value) use ($connection) {
                                        $value = (is_null($value) ? 'null' : $connection->quote($value));
                                });

                                $the_geom = 'null';
                                //$geom_names = [self::EDITOR_POINT, self::EDITOR_LINE_SHORT, self::EDITOR_POLYGON_SHORT];
                                if ($this->the_geom) {
                                        $the_geom = Text::prepare("ST_SetSRID(ST_GeomFromGeoJSON('%s'),4326)", $this->the_geom);
                                }
                                $data['the_geom'] = $the_geom;

                                $insertFields = implode(',', array_keys($data));
                                $insertValues = implode(',', array_values($data));
                                $insertQuery = "INSERT INTO {$table} ({$insertFields}) VALUES ({$insertValues}) RETURNING cartodb_id";

                                try {
                                        $insertData = Connectivity::runCurl($sqlApiUrl,
                                                                            array(CURLOPT_CUSTOMREQUEST => 'POST',
                                                                                  CURLOPT_FAILONERROR   => false,
                                                                                  CURLOPT_POSTFIELDS    => array('q'            => $insertQuery,
                                                                                                                 'api_key'      => $apiKey,
                                                                                                                 'format'       => 'json')));

                                        if ($insertData) {
                                                $insertContents = json_decode($insertData, true);

                                                if (isset($insertContents['total_rows']) && $insertContents['total_rows']) {
                                                        $row = reset($insertContents['rows']);
                                                        if ($row) $propositionId = $row['cartodb_id'];
                                                }
                                        }
                                } catch (Exception $e) {}
                        }
                }

                if ($propositionId) {
                        // Save to data layer when needed
                        if (!$editProposition || $this->visualization[REQUEST_PARAMETER_MYMAP]) {
                                $this->featureArray = [
                                        'visualizationId'       => $this->visualization[REQUEST_PARAMETER_VIZ_ID],
                                        'actions'               => $this->action,
                                        'layerId'               => $this->layerId,
                                        'featureId'             => $this->featureId,
                                        'the_geom'              => $this->the_geom,
                                        'columnData'            => [],
                                        'style'                 => $this->featureStyle
                                ];

                                if (in_array($this->action, [EDITOR_ACTION_NEW_FEATURE, EDITOR_ACTION_EDIT_DATA, Text::prepare('%s,%s',EDITOR_ACTION_EDIT_DATA, EDITOR_ACTION_EDIT_STYLE)])) {
                                        $this->featureArray['columnData']['name'] = ($featureName ? $featureName : '');
                                        $this->featureArray['columnData']['description'] = ($featureDescription ? $featureDescription : '');
                                        $this->featureArray['columnData']['imageurl'] = ($imageurl ? $imageurl : '');
                                }

                                $this->feature = json_encode($this->featureArray);

                                $webserviceUrl = WEBSERVICE_URL . 'visualization/wo/edit_layer';
                                $webserviceParams = array('user'        => WEBSERVICE_USER,
                                                          'password'    => WEBSERVICE_PASSWORD,
                                                          'userName'    => $this->user['Name'],
                                                          'userKey'     => $this->user['ApiKey'],
                                                          'editInfo'    => $this->feature,
                                                          'format'      => 'application/json');

                                $webserviceResult = Connectivity::runCurl($webserviceUrl,
                                                                          array(CURLOPT_CUSTOMREQUEST   => 'POST',
                                                                                CURLOPT_POSTFIELDS      => $webserviceParams));
                                if ($webserviceResult) {
                                        $webserviceContents = json_decode($webserviceResult, true);

                                        if (isset($webserviceContents['response']['edit_layer'])) {
                                                $result = $webserviceContents['response']['edit_layer'];
                                                if (isset($result['success']) && $result['success'])
                                                        $result['proposition'] = $propositionId;
                                        }

                                        $propositionName = $featureName;
                                        $propositionDescription = $propositionDescription;
                                        $propositionUser = $this->user['Name'];
                                        $propositionStatus = 'accepted';
                                        $updateId = $this->visualization[REQUEST_PARAMETER_VIZ_ID] . '-' . $propositionId;
                                        $updateUrl = Url::buildPlatformUrl(false, null, null, null, null,
                                                                            array(REQUEST_PARAMETER_UPDATE_ID    => $updateId));
                                        if ($this->featureId) {
                                                $queryPropositionUsers = Text::prepare("SELECT ('''' || string_agg(DISTINCT user_name, ''',''') || '''') as users FROM %s WHERE feature_id = %s AND NOT user_name IN ('%s','%s');", $table, $this->featureId, $propositionUser, $vizUser['username']);
                                                $sqlApiUrl = Text::prepare('http://%s.spotzi.me/api/v2/sql', $vizUser['username']);
                                                $propositionUsersResult = Connectivity::runCurl($sqlApiUrl,
                                                                                    array(CURLOPT_CUSTOMREQUEST    => 'POST',
                                                                                          CURLOPT_POSTFIELDS       => ['q' => $queryPropositionUsers]));

                                                if ($propositionUsersResult) {
                                                        $propositionUsersJSON = json_decode($propositionUsersResult, true);
                                                        if (isset($propositionUsersJSON['rows']) && $propositionUsersJSON['rows']) {
                                                                $mailingListUsers = $propositionUsersJSON['rows'][0]['users'];
                                                                if(!is_null($mailingListUsers)) {
                                                                        $queryEmails = Text::prepare("SELECT email FROM users WHERE username IN (%s)", $mailingListUsers);
                                                                        $emailResult = PostgreSqlDatabase::getInstance()->execute(self::DB_CONNECTION_CARTO_READ, $queryEmails);
                                                                        if ($emailResult) {
                                                                                $actions = explode(',', $this->action);

                                                                                $actionTypes = array();
                                                                                foreach ($actions as $action) {
                                                                                        switch ($action) {
                                                                                                case EDITOR_ACTION_NEW_FEATURE:
                                                                                                        $actionTypes[] = __('New feature');
                                                                                                        break;
                                                                                                case EDITOR_ACTION_EDIT_DATA:
                                                                                                        $actionTypes[] = __('Data edit');
                                                                                                        break;
                                                                                                case EDITOR_ACTION_EDIT_GEOM:
                                                                                                        $actionTypes[] = __('Geometry edit');
                                                                                                        break;
                                                                                                case EDITOR_ACTION_EDIT_STYLE:
                                                                                                        $actionTypes[] = __('Style edit');
                                                                                                        break;
                                                                                                case EDITOR_ACTION_DELETE:
                                                                                                        $actionTypes[] = __('Feature delete');
                                                                                                        break;
                                                                                        }
                                                                                }
                                                                                $actionType = implode(' / ', $actionTypes);

                                                                                ob_start();
                                                                                require_once($this->modulePath . DIR_VIEW . 'mail/update_proposition_concurrent.php');
                                                                                $messageConcurrent = ob_get_clean();
                                                                                // Prepare the updated user mailer
                                                                                Mail::addMailer(EMAIL_HOST, EMAIL_PORT, EMAIL_FROM, EMAIL_FROM_PASSWORD, BRAND_PRODUCT);
                                                                                foreach ($emailResult as $rowEmail) {
                                                                                        // Send the updated user email
                                                                                        Mail::send($rowEmail['email'], EMAIL_FROM, BRAND_PRODUCT . __(' - there was an update on the map you have proposed on'),
                                                                                                   $messageConcurrent, array(), array(), array(), true, true);
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        } else {
                                $result = array('success'       => true,
                                                'reload'        => (boolean) $this->the_geom,
                                                'proposition'   => $propositionId);
                        }

                        // Send email to the map owner and the map user when needed
                        if (isset($result['success']) && $result['success'] && !$this->visualization[REQUEST_PARAMETER_MYMAP]) {
                                $actions = explode(',', $this->action);
                                if (!is_array($actions)) $actions = array($this->action);

                                $actionTypes = array();
                                foreach ($actions as $action) {
                                        switch ($action) {
                                                case EDITOR_ACTION_NEW_FEATURE:
                                                        $actionTypes[] = __('New feature');
                                                        break;
                                                case EDITOR_ACTION_EDIT_DATA:
                                                        $actionTypes[] = __('Data edit');
                                                        break;
                                                case EDITOR_ACTION_EDIT_GEOM:
                                                        $actionTypes[] = __('Geometry edit');
                                                        break;
                                                case EDITOR_ACTION_EDIT_STYLE:
                                                        $actionTypes[] = __('Style edit');
                                                        break;
                                                case EDITOR_ACTION_DELETE:
                                                        $actionTypes[] = __('Feature delete');
                                                        break;
                                        }
                                }
                                $actionType = implode(' / ', $actionTypes);

                                $updateId = $this->visualization[REQUEST_PARAMETER_VIZ_ID] . '-' . $propositionId;
                                $updateUrl = Url::buildPlatformUrl(false, null, null, null, null,
                                                                   array(REQUEST_PARAMETER_UPDATE_ID    => $updateId));

                                // Prepare the update mailer
                                Mail::addMailer(EMAIL_HOST, EMAIL_PORT, EMAIL_FROM, EMAIL_FROM_PASSWORD, BRAND_PRODUCT);

                                // Retrieve the update owner email template
                                ob_start();
                                require_once($this->modulePath . DIR_VIEW . 'mail/update_owner.php');
                                $message = ob_get_clean();

                                // Send the update owner email
                                Mail::send($this->visualization['map_owner'], EMAIL_FROM,
                                           BRAND_PRODUCT . ($editProposition ? __(' - an update was proposed on your map') : __(' - an update was performed on your map')),
                                           $message, array(), array(), array(), true, true);

                                // Retrieve the update user email template
                                ob_start();
                                require_once($this->modulePath . DIR_VIEW . 'mail/update_user.php');
                                $message = ob_get_clean();

                                // Send the update user email
                                Mail::send($this->user['Email'], EMAIL_FROM,
                                           BRAND_PRODUCT . ($editProposition ? __(' - your map update proposition') : __(' - your map update')),
                                           $message, array(), array(), array(), true, true);
                        }
                }

                return array(REQUEST_RESULT     => $result);
        }
}