<!DOCTYPE html>
<html>
        <head>
                <title><?php echo BRAND_PRODUCT; ?></title>
                <!-- Styles -->
                <style>
                /* Fonts */

                @import url(https://fonts.googleapis.com/css?family=Open+Sans:300,400);

                /* General */

                html, body {
                        font-family: 'Open Sans', Sans-serif;
                        font-size: 13px;
                        font-weight: 300;
                }

                table {
                        border-collapse: collapse;
                }

                .spotziGreen {
                        color: #8db832 !important;
                }

                .spotziOrange {
                        color: #ffb400 !important;
                }

                .spotziRed {
                        color: #fb0000 !important;
                }

                .email {
                        width: 500px;
                }

                .email a {
                        color: #8db832;
                        text-decoration: none;
                }

                .email td {
                        vertical-align: top;
                }

                .email hr {
                        height: 1px;
                        color: #dddddd;
                }

                .email .logo {
                        text-align: right;
                }

                .email .hidden td {
                        color: #ffffff;
                }

                .email .hidden td::selection, .email .hidden td::-moz-selection {
                        color: #dddddd;
                }
                </style>
        </head>
        <body>
                <table class="email">
                        <tr>
                                <td><?php _e('Dear map owner,'); ?></td>
                                <td class="logo">
                                        <img src="<?php echo URL_BASE; ?>/img/spotzi/logo.png" alt="">
                                </td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td colspan="2">
                                        <?php ($editProposition ? _e('A %s user proposed an update on one of your maps.', BRAND_PRODUCT) : _e('A %s user performed an update on one of your maps.', BRAND_PRODUCT)); ?>
                                        <?php _e('Below you will find details of the update.'); ?>
                                </td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td colspan="2"><hr></td>
                        </tr>
                        <tr>
                                <td><?php _e('Map'); ?></td>
                                <td><?php echo $this->vizJSON['title']; ?></td>
                        </tr>
                        <tr>
                                <td><?php _e('User'); ?></td>
                                <td><a href="mailto:<?php echo $this->user['Email']; ?>?subject=<?php ($editProposition ? _e('Your update proposition on map %s', $this->vizJSON['title']) : _e('Your update on map %s', $this->vizJSON['title'])); ?>"><?php echo ucfirst($this->user['Name']); ?></a></td>
                        </tr>
<?php if ($actionType): ?>
                        <tr>
                                <td><?php _e('Update type'); ?></td>
                                <td><?php echo $actionType; ?></td>
                        </tr>
<?php endif; ?>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td class="spotziOrange"><?php _e('Update ID'); ?></td>
                                <td><?php echo $updateId; ?></td>
                        </tr>
<?php if ($featureName || $propositionDescription): ?>
<?php   if ($featureName): ?>
                        <tr>
                                <td><?php _e('Name'); ?></td>
                                <td><?php echo $featureName; ?></td>
                        </tr>
<?php   endif; ?>
<?php   if ($propositionDescription): ?>
                        <tr>
                                <td><?php _e('Description'); ?></td>
                                <td><?php echo nl2br($propositionDescription); ?></td>
                        </tr>
<?php   endif; ?>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
<?php endif; ?>
                        <tr>
                                <td colspan="2">
                                        <?php ($editProposition ? _e('You can review the update by <a href="%s">clicking here</a>.', $updateUrl) : _e('You can view the update by <a href="%s">clicking here</a>.', $updateUrl)); ?>
                                        <?php _e('The update will be listed in the map updates window. Be aware: you may need to log in to your Geonovum account again in order to view the update.'); ?>
                                </td>
                        </tr>
                        <tr>
                                <td colspan="2"><hr></td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td colspan="2"><?php _e('Best regards,'); ?></td>
                        </tr>
                        <tr>
                                <td colspan="2"><b><?php echo BRAND_NAME; ?></b></td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td colspan="2"><?php _e('Veilingdreef 17'); ?></td>
                        </tr>
                        <tr>
                                <td colspan="2"><?php _e('4614RX, Bergen op Zoom'); ?></td>
                        </tr>
                        <tr>
                                <td colspan="2"><?php _e('The Netherlands'); ?></td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td colspan="2"><?php _e('+31 164 24 00 00'); ?></td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                        </tr>
                        <tr>
                                <td colspan="2">
                                        <a id="website" href="<?php _e('http://www.spotzi.com/en/'); ?>"><?php _e('Website'); ?></a>
                                        |
                                        <a id="contact" href="mailto:<?php _e('info@spotzi.com'); ?>"><?php _e('Contact'); ?></a>
                                        |
                                        <a id="problem" href="mailto:<?php echo EMAIL_FROM; ?>?subject=<?php _e('%s - problem with Spotzi Geonovum account', BRAND_PRODUCT); ?>"><?php _e('Report a problem'); ?></a>
                                </td>
                        </tr>
                </table>
        </body>
</html>