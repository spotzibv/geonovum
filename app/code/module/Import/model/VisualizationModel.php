<?php
/**
 * Class VisualizationModel - Import visualization model.
 *
 * @category    Geonovum
 * @package     Module
 * @subpackage  Import
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class VisualizationModel extends ImportModel {
        // User variable
        public $user                            = array();

        public function validateRequestParams() {
                $loggedIn = Session::getData(REQUEST_PARAMETER_LOGGEDIN);
                $this->user = Session::getData(REQUEST_PARAMETER_USER_NAME);
                if (!$loggedIn || !isset($this->user['Name'], $this->user['Email']))
                        ErrorHandler::error(E_ERROR, 'This action is not allowed');
        }

        public function import() {
                // Set the script execution settings
                $importSize = Text::formatBytes(VISUALIZATION_IMPORT_SIZE, 'mB');
                setExecutionSettings($importSize + 256);

                $allowedFiles = explode(',', str_replace('.', '', UPLOAD_FILES_GEO));

                $result = false;
                $error = null;
                $fileName = File::handleUpload(DIR_TEMP, 'importFile', null, $allowedFiles, VISUALIZATION_IMPORT_SIZE);
                if ($fileName) {
                        $fileInfo = pathinfo($fileName);
                        $fileNameClean = preg_replace(array('/[^a-zA-Z0-9\s]/', '/\s+/'), ' ', strtolower($fileInfo['filename']));
                        $fileNameNew = str_replace(' ', '_', trim(substr($fileNameClean, 0, 22))) . '_' . Date::format('now', 'Ymd_His') . '.' . $fileInfo['extension'];

                        $destinationDir = '\\\\db-images\data.spotzi.com\import\\' . $this->user['Name'];
                        if (!is_dir($destinationDir)) mkdir($destinationDir);
                        $destination = $destinationDir . '\\' . $fileNameNew;

                        if (is_dir($destinationDir) && copy(DIR_TEMP . $fileName, $destination)) {
                                $importName = $this->getParam('importName');
                                if (!$importName) $importName = ucwords(substr(str_replace('_', ' ', $fileName), 0, strrpos($fileName, '.')));
                                $dataUrl = 'http://data.spotzi.com/import/' . $this->user['Name'] . '/' . $fileNameNew;

                                $this->vizDb->insert(self::DB_CONNECTION_VIZ_WRITE, 'VisualizationImport',
                                                     array('Service'            => 'geonovum',
                                                           'UserName'           => $this->user['Name'],
                                                           'Email'              => $this->user['Email'],
                                                           'Name'               => $importName,
                                                           'DataUrl'            => $dataUrl,
                                                           'DebugImport'        => debugMode()));

                                $webserviceUrl = Text::prepare('%svisualization/wo/import?user=%s&password=%s&userName=%s&userKey=%s&format=application/json&callback=%s',
                                                                       WEBSERVICE_URL, WEBSERVICE_USER, WEBSERVICE_PASSWORD, $this->user['Name'], $this->user['ApiKey'],
                                                                       urlencode(Url::buildPlatformURL(false, 'import', 'import', 'finish',
                                                                                                       array(REQUEST_PARAMETER_SESSION_ID       => Session::id()))));
                                Connectivity::runCurlAsync($webserviceUrl);

                                $result = true;
                        } else {
                                $error = __('An error occured while preparing the file');
                        }

                        File::delete(DIR_TEMP . $fileName);
                } else {
                        $error = __('An error occured while uploading the file');
                }

                if ($result === false)
                        ErrorHandler::error(E_NOTICE, "The import failed, file name: %s\nerror: %s", $fileName, $error);

                return array(REQUEST_RESULT     => $result,
                             REQUEST_ERROR      => $error);
        }
}