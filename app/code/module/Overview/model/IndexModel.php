<?php
/**
 * Class IndexModel - Overview index model.
 *
 * @category    Geonovum
 * @package     Module
 * @subpackage  Visualization
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class IndexModel extends ModuleModel {
        // Visualization variable
        public $visualization                           = array();
        public $visualizationSet                        = true;

        /**
         * Constructor.
         *
         * @param       Request         $request        Request object
         */
        public function __construct(&$request) {
                // Construct the parent class
                parent::__construct($request);

                $this->prepareVisualization();
        }

        public function validateRequestParams() {

        }

        /**
         *
         * @return string
         */
        protected function prepareVisualization() {
                $loggedIn = Session::getData(REQUEST_PARAMETER_LOGGEDIN);
                $sessionViz = Session::getData(REQUEST_PARAMETER_VIZ);
                $user = Session::getData(REQUEST_PARAMETER_USER_NAME);

                $visualization = array('communityMaps'  => array(),
                                       'myMaps'         => array());

                // Community maps
                $webserviceUrl = Text::prepare('%svisualization/wo/community?user=%s&password=%s&userName=%s&userKey=%s&format=application/json',
                                               WEBSERVICE_URL, WEBSERVICE_USER, WEBSERVICE_PASSWORD, $user['Name'], $user['ApiKey']);

                $requestContents = Connectivity::runCurl($webserviceUrl);
                if ($requestContents) {
                        $jsonOutput = json_decode($requestContents, true);

                        if (isset($jsonOutput['response']['community'])) $visualization['communityMaps'] = $jsonOutput['response']['community'];
                }

                // My maps
                if ($loggedIn) {
                        $webserviceUrl = Text::prepare('%svisualization/wo/visualization?user=%s&password=%s&userName=%s&userKey=%s&format=application/json',
                                                       WEBSERVICE_URL, WEBSERVICE_USER, WEBSERVICE_PASSWORD, $user['Name'], $user['ApiKey']);

                        $requestContents = Connectivity::runCurl($webserviceUrl);
                        if ($requestContents) {
                                $jsonOutput = json_decode($requestContents, true);

                                if (isset($jsonOutput['response']['visualization'])) $visualization['myMaps'] = $jsonOutput['response']['visualization'];
                        }
                }

                // Set the default visualization from the update ID
                $updateId = $this->getParam(REQUEST_PARAMETER_UPDATE_ID);
                if ($updateId) {
                        $lastDashPos = strrpos($updateId, '-');
                        if ($lastDashPos !== false) {
                                $vizId = substr($updateId, 0, $lastDashPos);

                                $defaultVisualization = Collection::keyValueSearch($visualization['communityMaps'], 'Id', $vizId);
                                if ($defaultVisualization) {
                                        $visualization['defaultVisualization'] = reset($defaultVisualization);
                                } else {
                                        $defaultVisualization = Collection::keyValueSearch($visualization['myMaps'], 'Id', $vizId);
                                        if ($defaultVisualization) $visualization['defaultVisualization'] = reset($defaultVisualization);
                                }
                        }
                }

                // Set the default visualization from cache
                $vizUrl = null;
                if (empty($visualization['defaultVisualization'])) {
                        if (isset($sessionViz[REQUEST_PARAMETER_VIZ_URL])) $vizUrl = $sessionViz[REQUEST_PARAMETER_VIZ_URL];
                        if ($vizUrl) {
                                $defaultVisualization = Collection::keyValueSearch($visualization['communityMaps'], 'Url', $vizUrl);
                                if ($defaultVisualization) {
                                        $visualization['defaultVisualization'] = reset($defaultVisualization);
                                } else {
                                        $defaultVisualization = Collection::keyValueSearch($visualization['myMaps'], 'Url', $vizUrl);
                                        if ($defaultVisualization) $visualization['defaultVisualization'] = reset($defaultVisualization);
                                }
                        }
                }

                // Fallback for the default visualization
                if (empty($visualization['defaultVisualization'])) {
                        $visualization['defaultVisualization'] = array('Url'    => ($vizUrl ? $vizUrl : VISUALIZATION_DEFAULT));

                        if (!$vizUrl) $this->visualizationSet = false;
                }

                $this->visualization = $visualization;
        }
}