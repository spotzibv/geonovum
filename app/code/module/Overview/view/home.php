<script>
var communityMaps = <?php echo json_encode($model->visualization['communityMaps']); ?>;
var myMaps = <?php echo json_encode($model->visualization['myMaps']); ?>;

$(document).ready(function() {
        // Home screen
        $('#homeButton').click(showHomeScreen);
        $('#homeMap, #homeClose').click(hideHomeScreen);
        $('#homeSub').on('show', '> div', function() {
                updateScrolls($('#home'));
        });

        // Search
        $('#homeSearchForm').submit(searchContent);

        // Map tab
        $('#homeMaps').on('show', updateMapTab).on('show', function(e) {
                $(this).off(e);
                $('#<?php echo ($loggedIn ? 'menuLogin' : 'menuPopular'); ?>').click();
        });
        $('#maps').on('click', '.mapBlock', function() {
                var value = $(this).data('value');
                if (value) {
                        if (value.hasOwnProperty('Url') && value['Url']) {
                                openVisualization(value);
                        } else {
                                parentMapCategory = activeMapCategory;
                                activeMapCategory = value;
                                updateMapTab();
                        }
                }
        });
        $('#mapMenu').on('click', 'a[data-maps]', showMapTab);
<?php if ($loggedIn): ?>

        // Content
        $('#importForm').submit(importStart);
        $('#importNext').click(function() {
                $('#importSuccess').hide();
                $('#importContent').show();
                $('#importName').focus();
        });
        $('#importClose').click(function() {
                closeModal('importData');
        });
        $('#createForm').submit(createData);
        $('#mapsLogin').on('click', '.delete', deleteData);
<?php endif; ?>
});

// Home screen
function showHomeScreen(screen) {
        $('#homeSub').find('> div').hide(function() {
                $(this).trigger('hide');
        }).end().find('> div[data-screen=' + (typeof screen === 'string' ? screen : 'maps') + ']').show(function() {
                $(this).trigger('show');
        });

        if (!$('#home').is(':visible')) {
                $('#menu, #content').slideUp();
                $('.leaflet-container').find('> :not(.leaflet-map-pane)').css('visibility', 'hidden');

                $('#home').slideDown();
        }

        updateScrolls($('#home'));
}

function hideHomeScreen() {
        $('#home').slideUp();

        $('#menu').slideDown(400, function() {
                $(this).trigger('custom');
        });

        $('.leaflet-container').find('> :not(.leaflet-map-pane)').css('visibility', 'visible');
}

// Map tab
var activeMapTab, activeMapCategory, parentMapCategory;

function updateMapTab() {
        if (activeMapTab) {
                activeMapTab.empty().hide();

                switch (activeMapTab.attr('id')) {
                        case 'mapsCommunity':
                                updateMapTabContent(communityMaps, function() {
                                        var elemCreate = $('<li class="tableView mapBlock green" title="<?php _e('Create your own map'); ?>">\n\
                                                                <div class="tableCellView">\n\
                                                                        <span class="fa-icon fa-plus-circle"></span>\n\
                                                                        <h6 class=descriptionBlock><?php _e('Create your own map'); ?></h6>\n\
                                                                </div>\n\
                                                        </li>').click(function() {
                                                checkLogin(function() {
                                                        $('#menuLogin').click();
                                                });
                                        });
                                        activeMapTab.prepend(elemCreate);
                                });
                                break;
                        case 'mapsLogin':
                                checkLogin(function() {
<?php if ($loggedIn): ?>
                                        myMaps.sort(function(a, b) {
                                                var aName = a['Title'].toLowerCase();
                                                var bName = b['Title'].toLowerCase();
                                                return (aName < bName ? -1 : (aName > bName ? 1 : 0));
                                        });
                                        updateMapTabContent(myMaps, function() {
                                                var elemNew = $('<li class="tableView mapBlock green" title="<?php _e('Create map'); ?>">\n\
                                                                        <div class="tableCellView">\n\
                                                                                <span class="fa-icon fa-plus-circle"></span>\n\
                                                                                <h6 class=descriptionBlock><?php _e('Create map'); ?></h6>\n\
                                                                        </div>\n\
                                                                </li>').click(function() {
                                                        openModal('createData');
                                                        $('#createName').focus();
                                                });
                                                var elemImport = $('<li class="tableView mapBlock yellow" title="<?php _e('Import data'); ?>">\n\
                                                                        <div class="tableCellView">\n\
                                                                                <span class="fa-icon fa-cloud-upload"></span>\n\
                                                                                <h6 class=descriptionBlock><?php _e('Import data'); ?></h6>\n\
                                                                        </div>\n\
                                                                </li>').click(function() {
                                                        openModal('importData');
                                                        $('#importName').focus();
                                                });
                                                activeMapTab.prepend(elemNew, elemImport);
                                        });
<?php endif; ?>
                                }, true);
                                break;
                }

                setImageError(activeMapTab, '<?php echo VISUALIZATION_PLACEHOLDER; ?>');
                activeMapTab.fadeIn();
                updateScrolls($('#home'));
        }
}

function updateMapTabContent(objects, callback) {
        var fragment = $(document.createDocumentFragment());
        $.each(objects, function(index, value) {
                if ($.isPlainObject(value)) {
                        var title = value['Title'].capitalize();
                        var titleHTML = (value.hasOwnProperty('CategoryId') ? title : '<div class="title">' + title + '</div><div class="fa-icon fa-times delete"></div>');

                        if (!value.hasOwnProperty('IsSubCategory')) {
                                var vizId = value['Url'].split('/')[6];
                                value['ImageUrl'] = '<?php echo VISUALIZATION_IMAGE_URL; ?>' + vizId + '.jpg';
                        }
                        var active = (visualization.<?php echo REQUEST_PARAMETER_VIZ_URL; ?>  === value['Url'] ? ' active' : '');

                        fragment.append($('<li class="tableView mapBlock' + active + '" title="' + title + '">\n\
                                                <div class="tableCellView">\n\
                                                        <img src="' + value['ImageUrl'] + '" alt="" />\n\
                                                        <h6 class="titleBlock">' + titleHTML + '</h6>\n\
                                                </div>\n\
                                        </li>').data('value', value));
                }
        });
        activeMapTab.empty().append(fragment);

        if ($.isFunction(callback)) callback();
}

function showMapTab() {
        var elem = $(this);

        $('#mapMenu').find('a').removeClass('active');
        elem.addClass('active');
        $('#maps').find('> ul').hide();

        activeMapTab = $('#' + elem.data('maps')).show();
        activeMapCategory = null, parentMapCategory = null;
        updateMapTab();
}

function openVisualization(value, callback) {
        hideHomeScreen();

        initializeVisualization(value['Url'], {}, function() {
                $(document).prop('title', '<?php echo BRAND_PRODUCT; ?> - ' + value['Title']);

                if ($.isFunction(callback)) callback();
        });
}

// Search
var lastSearch, markerSearch;

function searchContent(e) {
        e.preventDefault();

        var form = $(this);
        var text = form.find('input[type=text]');
        var submit = form.find('input[type=submit]');

        var search = text.val().toLowerCase();
        if (search !== '' && search.length >= 3) {
                showHomeScreen('results');

                if (search !== lastSearch) {
                        submit.prop('disabled', true);
                        lastSearch = search;

                        var dataDone, addressDone, visualizationsDone;
                        var callback = function() {
                                if (dataDone && addressDone && visualizationsDone)
                                        submit.prop('disabled', false);
                        };

                        searchData(search, function() {
                                dataDone = true;

                                callback();
                        });
                        searchAddress(search, function() {
                                addressDone = true;

                                callback();
                        });
                        searchVisualizations(search, function() {
                                visualizationsDone = true;

                                callback();
                        });
                }
        }
}

function searchData(search, callback) {
        var dataResults = $('#dataResults');
        dataResults.empty().removeClass('empty error');

        loadUrl('<?php echo $this->getExportDataURL(); ?>', {
                data: {
                        <?php echo REQUEST_PARAMETER_QUERY; ?>: search,
                        <?php echo REQUEST_PARAMETER_CONTENT_LIMIT; ?>: <?php echo VISUALIZATION_SEARCH_LIMIT; ?>
                }
        }, function(response) {
                if ($.isArray(response) && response.length) {
                        var regex = new RegExp('(' + search + ')', 'i');
                        var fragment = $(document.createDocumentFragment());
                        $.each(response, function(index, value) {
                                var fields = [];

                                $.each(value, function(fieldIndex, fieldValue) {
                                        if (regex.test(fieldValue))
                                                fields.push(fieldValue.replace(regex, '<b>$1</b>'));
                                });

                                if (fields.length) {
                                        fragment.append($('<li class="tableView">\n\
                                                                <a class="tableCellView">' + fields.join(', ') + '</a>\n\
                                                        </li>').click(function() {
                                                if (visualization.<?php echo REQUEST_PARAMETER_VIZ_LAYER_TYPE; ?> === 'layergroup') {
                                                        var layer = mapLayers[visualization.<?php echo REQUEST_PARAMETER_VIZ_LAYER_INDEX; ?>];
                                                        layer.trigger('featureClick', null, [value.centery, value.centerx], null, { cartodb_id: value.cartodb_id }, mapLayer.index || 0);

                                                        hideHomeScreen();
                                                }
                                        }));
                                }
                        });
                        dataResults.empty().append(fragment);
                } else {
                        dataResults.addClass('empty').html('<li><?php _e('No results were found'); ?></li>');
                }
        }, function() {
                dataResults.addClass('error').html('<li><?php _e('An error occured, please try again later'); ?></li>');
        }, callback);
}

function searchAddress(search, callback) {
        var addressResults = $('#addressResults');
        addressResults.empty().removeClass('empty error');

        clearSearchAddress();
        loadUrl('<?php echo $this->getLocationSearchURL(); ?>', {
                data: {
                        <?php echo REQUEST_PARAMETER_BOUNDINGBOX; ?>: (map ? map.getBounds().toBBoxString() : '-180,90,180,-90'),
                        <?php echo REQUEST_PARAMETER_QUERY; ?>: search
                }
        }, function(response) {
                if ($.isArray(response) && response.length) {
                        var regex = new RegExp('(' + search + ')', 'i');
                        var fragment = $(document.createDocumentFragment());
                        $.each(response, function(index, value) {
                                var addressName = value.<?php echo REQUEST_PARAMETER_LOCATION_NAME; ?>;
                                var displayName = addressName.capitalize().replace(regex, '<b>$1</b>');

                                fragment.append($('<li class="tableView">\n\
                                                        <a class="tableCellView">' + displayName + '</a>\n\
                                                </li>').click(function() {
                                        clearSearchAddress();
                                        setMapView(value.<?php echo REQUEST_PARAMETER_X; ?>, value.<?php echo REQUEST_PARAMETER_Y; ?>, <?php echo VISUALIZATION_ZOOM_DEFAULT; ?>);

                                        markerSearch = createMarker(value.<?php echo REQUEST_PARAMETER_X; ?>, value.<?php echo REQUEST_PARAMETER_Y; ?>).on('click', clearSearchAddress);
                                        map.addLayer(markerSearch);

                                        hideHomeScreen();
                                }));
                        });
                        addressResults.empty().append(fragment);
                } else {
                        addressResults.addClass('empty').html('<li><?php _e('No results were found'); ?></li>');
                }
        }, function() {
                addressResults.addClass('error').html('<li><?php _e('An error occured, please try again later'); ?></li>');
        }, callback);
}

function clearSearchAddress() {
        if (markerSearch) {
                map.removeLayer(markerSearch);
                markerSearch = null;
        }
}

function searchVisualizations(search, callback) {
        var visualizationResults = $('#visualizationResults');
        visualizationResults.empty().removeClass('empty error');

        var regex = new RegExp('(' + search + ')', 'i');
        var fragment = $(document.createDocumentFragment());
        $.each($.extend({}, communityMaps, myMaps), function(index, value) {
                if ($.isPlainObject(value) && regex.test(value['Title'] + ',' + value['Tags'])) {
                        var addressName = value['Title'];
                        var displayName = addressName.capitalize().replace(regex, '<b>$1</b>');

                        var vizId = value['Url'].split('/')[6];
                        var imageUrl = '<?php echo VISUALIZATION_IMAGE_URL; ?>' + vizId + '.jpg';

                        fragment.append($('<li class="tableView">\n\
                                                <div class="tableCellView visualizationImage">\n\
                                                        <img src="' + imageUrl + '" alt="" />\n\
                                                </div>\n\
                                                <a>' + displayName + '</a>\n\
                                        </li>').click(function() {
                                openVisualization(value);
                        }));
                }
        });
        visualizationResults.empty().append(fragment);

        if (visualizationResults.is(':empty')) {
                visualizationResults.addClass('empty').html('<li><?php _e('No results were found'); ?></li>');
        } else {
                setImageError(visualizationResults, '<?php echo VISUALIZATION_PLACEHOLDER; ?>');
        }

        if ($.isFunction(callback)) callback();
}
<?php if ($loggedIn): ?>

// Content
function importStart() {
        $('#importError').hide();

        $('#importFrame').off('load').on('load', importFinish);
        $(this).find('input[type=submit]').attr('disabled', true).end().find('.loading').show();
}

function importFinish() {
        var response = $(this).contents().find('body').html();
        var responseJSON = false;
        try {
                responseJSON = $.parseJSON(response);
        } catch (err) {}

        if (responseJSON && responseJSON.hasOwnProperty('<?php echo REQUEST_RESULT; ?>') &&
            responseJSON.<?php echo REQUEST_RESULT; ?> === true) {
                $('#importContent').hide();
                $('#importSuccess').show();
                $('#importForm').trigger('reset');

                $.localStorage.setItem('home', true);
        } else {
                $('#importError').html(responseJSON.hasOwnProperty('<?php echo REQUEST_ERROR; ?>') ? responseJSON.<?php echo REQUEST_ERROR; ?> : '<?php _e('An unexpected error has occured.<br>If this error keeps occuring, please contact your vendor for assistance.'); ?>').show();

                logError(response);
        }

        $('#importForm').find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
}

function createData(e) {
        e.preventDefault();
        $('#createError').hide();

        var form = $(this);
        form.find('input[type=submit]').attr('disabled', true).end().find('.loading').show();

        loadUrl(form.attr('action'), {
                data: form.serializeObject()
        }, function(response) {
                closeModal('createData');

                openVisualization(response, function() {
                        myMaps.push(response);
                });
        }, function(err) {
                $('#createError').show();

                logError(err);
        }, function() {
                form.find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
        });
}

function deleteData(e) {
        e.preventDefault();
        e.stopPropagation();

        if (confirm('<?php _e('Are you sure you want to delete this map permanently?\nThis cannot be undone.'); ?>')) {
                var value = $(this).closest('.mapBlock').data('value');
                var visualizationId = value['Id'];

                if (value.hasOwnProperty('Url')) {
                        loadUrl('<?php echo $this->getVisualizationDeleteUrl(); ?>', {
                                data: {
                                        <?php echo REQUEST_PARAMETER_VIZ_ID; ?>: visualizationId,
                                        <?php echo REQUEST_PARAMETER_VIZ_URL; ?>: value['Url']
                                }
                        }, function() {
                                if (visualization.<?php echo REQUEST_PARAMETER_VIZ_ID ?> === visualizationId)
                                        openVisualization(myMaps[0]);

                                removeBySubValue(myMaps, 'Id', visualizationId);
                                $('#menuLogin').click();
                        });
                }
        }
}
<?php endif; ?>
</script>
<div id="home">
        <div id="homeMap"></div>
        <div id="homeWindow">
                <div id="homeClose" class="fa-icon fa-times"></div>
                <div id="homeMenu">
                        <div id="homeSearch" class="menuSearch">
                                <form id="homeSearchForm" class="menuSearchForm" method="post" enctype="multipart/form-data" action="#">
                                        <div id="homeButton" title="<?php _e('Home'); ?>">
                                                <span class="fa-icon fa-bars"></span>
                                        </div>
                                        <div id="homeSearchText" class="menuSearchText">
                                                <input type="text" placeholder="<?php _e('Search for an address or a theme'); ?>" value="">
                                        </div>
                                        <div id="homeSearchSubmit" class="menuSearchSubmit">
                                                <input type="submit" value="&#xf002;">
                                        </div>
                                </form>
                        </div>
                </div>
                <div id="homeSub" class="scroll">
                        <div id="homeMaps" data-screen="maps">
                                <div id="mapMenu" class="clear">
                                        <a id="menuCommunity" data-maps="mapsCommunity"><?php _e('Community Maps'); ?></a>
                                        <a id="menuLogin" data-maps="mapsLogin"><?php _e('My Maps'); ?></a>
                                </div>
                                <div id="maps" class="scroll">
                                        <ul id="mapsCommunity"></ul>
                                        <ul id="mapsLogin"></ul>
                                </div>
                        </div>
                        <div id="homeResults" data-screen="results">
                                <div id="dataArea">
                                        <h4><?php _e('Data'); ?></h4>
                                        <ul id="dataResults"></ul>
                                </div>
                                <div id="visualizationArea">
                                        <h4><?php _e('Map'); ?></h4>
                                        <ul id="visualizationResults"></ul>
                                </div>
                                <div id="addressArea">
                                        <h4><?php _e('Address'); ?></h4>
                                        <ul id="addressResults"></ul>
                                </div>
                        </div>
                </div>
        </div>
        <div id="homeModal">
<?php if ($loggedIn):
        $size = Text::formatBytes(VISUALIZATION_IMPORT_SIZE, 'gB'); ?>
                <div id="importData" data-remodal-id="importData">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('Import data'); ?></h4>
                        <div id="importContent">
                                <p class="description"><?php _e('Please choose the file to import to your Spotzi Geonovum account.<br><br><span class="spotziOrange">Beware!</span> In doubt about whether the file meets the requirements, or did the import fail? Check the <a href="http://www.spotzi.com/en/help/documentation/import-dataset/#shape" target="_blank">user guide</a> to see if the file type and - when applicable - the coordinate system of the data are supported by the Spotzi Geonovum.'); ?></p>
                                <form id="importForm" method="post" enctype="multipart/form-data" action="<?php echo $this->getImportVisualizationUrl(); ?>" target="importFrame">
                                        <p id="importError" class="spotziRed" style="display: none;"></p>
                                        <p><input type="text" id="importName" name="importName" placeholder="<?php _e('Map name'); ?>"></p>
                                        <p><input type="file" id="importFile" name="importFile" accept="<?php echo UPLOAD_FILES_GEO; ?>" required></p>
                                        <p class="note clear">
                                                <span><?php _e('Limit: %s, file types: CSV, XLS(X), GeoJSON, ZIP, KML, GPX', ($size ? $size . 'GB' : __('none'))); ?></span>
                                                <input type="submit" value="<?php _e('Import data'); ?>">
                                                <iframe id="importFrame" name="importFrame" style="display: none;"></iframe>
                                        </p>
                                        <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                                </form>
                                <p class="note borderTop"><?php _e('New to Spotzi? Check the <a href="http://www.spotzi.com/en/help/documentation/" target="_blank">user guide</a> to get started.'); ?></p>
                        </div>
                        <div id="importSuccess" style="display: none;">
                                <p id="description" class="spotziGreen"><?php _e('The file was uploaded successfully, you will receive email once the import process is finished. Afterwards you will have to refresh this page in order to find the map in "My Maps".'); ?></p>
                                <p class="note clear">
                                        <input id="importClose" type="button" value="<?php _e('Close'); ?>">
                                        <input id="importNext" type="button" value="<?php _e('Import another file'); ?>">
                                </p>
                        </div>
                </div>
                <div id="createData" data-remodal-id="createData">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('Create map'); ?></h4>
                        <div id="createDataContent">
                                <p class="description"><?php _e('Please pick a name for your new map.'); ?></p>
                                <form id="createForm" method="post" enctype="multipart/form-data" action="<?php echo $this->getVisualizationCreateUrl(); ?>" target="createFrame">
                                        <p id="createError" class="spotziRed" style="display: none;"><?php _e('An error occured while creating the map'); ?></p>
                                        <p><input type="text" id="createName" name="createName" placeholder="<?php _e('Map name'); ?>"></p>
                                        <p class="note clear">
                                                <span><?php _e('Afterwards you can add data to the map'); ?></span>
                                                <input type="submit" value="<?php _e('Create map'); ?>">
                                        </p>
                                        <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                                </form>
                        </div>
                </div>
<?php endif; ?>
        </div>
</div>