<script>
var loggedIn = <?php echo ($loggedIn ? 'true' : 'false'); ?>;

$(document).ready(function() {
        $('select').chosen({
                disable_search_threshold: 10,
                no_results_text: '<?php _e('No results were found'); ?>',
                placeholder_text_single: '<?php _e('Select...'); ?>',
                placeholder_text_multiple: '<?php _e('Select...'); ?>'
        });

        // Trigger handlers
        $('body').addClass('menu').on('initializeVisualization', function() {
                $('#menuHome').show();

                if (visualization.map_enabled || visualization.<?php echo REQUEST_PARAMETER_MYMAP; ?>) {
                        $('#menuMapSettings, #mapEdit')[visualization.<?php echo REQUEST_PARAMETER_MYMAP; ?> ? 'show' : 'hide']();
                        $('#menuMapUpdates, #menuMapEdit')[visualization.edit_enabled || visualization.<?php echo REQUEST_PARAMETER_MYMAP; ?> ? 'show' : 'hide']();

                        $('#updatesMenu').find('> li').removeClass('active');
                        $(visualization.<?php echo REQUEST_PARAMETER_MYMAP; ?> ? '#menuRecent' : '#menuMy').addClass('active');

                        if (visualization.<?php echo REQUEST_PARAMETER_MYMAP; ?>) {
                                $('#mapPrivacy').val(visualization.map_privacy).trigger('chosen:updated');
                                $('#mapUserInput').find('input').val(visualization.map_privacy_users);
                                $('#editPrivacy').val(visualization.edit_privacy).trigger('chosen:updated');
                                $('#editUserInput').find('input').val(visualization.edit_privacy_users);
                                $('#editProposition').prop('checked', visualization.edit_proposition);
                                $('#editPropositionPrivate').prop('checked', visualization.edit_proposition_private);

                                $('#mapPrivacy, #editPrivacy, #editPropositionContent').change();
                        }

                        $('#updatesMenu').find('.active').click();
                } else {
                        $('#menuMapUpdates, #menuMapSettings, #menuMapEdit').hide();
                }
<?php if ($loggedIn): ?>

                initializeUpdate();
<?php endif; ?>
        }).on('clearVisualization', function() {
<?php if ($loggedIn): ?>
                hideUpdate();
<?php endif; ?>
        }).on('initializeMenu', function() {
                var fragment = window.location.hash;
                var modalId = (fragment ? fragment.slice(1) : $.localStorage.getItem('modal'));
                if (modalId<?php if ($loggedIn): ?> && modalId !== 'login'<?php endif; ?>) openModal(modalId);

                $('#<?php echo ($loggedIn ? 'menuLogin' : 'menuCommunity'); ?>').click();
<?php if (!$model->visualizationSet): ?>
                showHomeScreen();
<?php else: ?>
                ($.localStorage.getItem('home') ? showHomeScreen() : hideHomeScreen());
<?php endif; ?>
        });

        // Login
        $('#loginForm').submit(login);
        $('#passwordForm').submit(sendPasswordMail);
<?php if ($loggedIn): ?>
        $('#loginUserName').val('<?php echo $user['Name']; ?>');
        $('#menuUser').click(function() {
                checkLogin(function() {
                        openModal('account');
                });
        });
        $('#menuMaps').click(function() {
                checkLogin(function() {
                        $('#menuLogin').click();
                        showHomeScreen();
                });
        });
        $('#menuLogout').click(logout);
        $('#accountPasswordForm').submit(changeAccountPassword);

        $('#menuMapUpdates').click(function() {
                checkLogin(function() {
                        openModal('mapUpdates');

                        $('#menuRecent, #menuPending')[visualization.edit_proposition_private ? 'hide' : 'show']();
                        $('#updatesMenu').find('.active').removeClass('active').click();
                });
        });
        $('#updatesSearchForm').submit(function(e) {
                e.preventDefault();

                $('#menuResults').removeClass('active').show().click();
        });
        $('#updatesMenu').on('click', '> li', showMapUpdatesTab);
        $('#mapUpdates').on('click', '.dataTable tr', function() {
                openUpdate($(this).data('rel'));
        });
        $('#updateContainer').on('click', '#updateClose', hideUpdate);
        $('#menuMapSettings').click(function() {
                checkLogin(function() {
                        openModal('mapSettings');
                });
        });
        $('#settingsForm').submit(setMapSettings);
        $('#mapPrivacy').change(function() {
                var value = $(this).val();

                $('#mapUserInput')[value === 'link' ? 'slideDown' : 'slideUp']();
                $('#editPrivacyContent')[value === 'private' ? 'slideUp' : 'slideDown']();
                $('#editPropositionContent')[value === 'private' || $('#editPrivacy').val() === 'private' ? 'slideUp' : 'slideDown']();
        });
        $('#editPrivacy').change(function() {
                var value = $(this).val();

                $('#editUserInput')[value === 'link' ? 'slideDown' : 'slideUp']();
                $('#editPropositionContent')[value === 'private' ? 'slideUp' : 'slideDown']();
        });
        $('#editPropositionContent').change(function() {
                var checked = $('#editProposition').is(':checked');

                $('#editPropositionPrivate').parent().parent()[checked ? 'slideDown' : 'slideUp']();
        });

        setInterval(function() {
                loadUrl('<?php echo $this->getSessionStatusURL(); ?>', {}, function() {
                        loggedIn = true;
                }, function() {
                        loggedIn = false;
                }, function() {
                        if (!loggedIn) logoutSession();
                });
        }, 600000);
<?php else: ?>
        $('#menuUser, #menuLogin').click(function() {
                checkLogin(null, 'maps');
        });

        // Register
        $('#menuRegister').click(showRegister);
        $('#registerForm').submit(register);
<?php endif; ?>

        // Search
        $('#menuSearchForm').submit(function(e) {
                e.preventDefault();

                $('#homeSearchForm').find('input[type=text]').val($(this).find('input[type=text]').val())
                        .end().submit();
        });

        // Menu
        $(document).click(function() {
                $('.menuSub').removeClass('active');
        });
        $('.menuButton').has('.menuSub').click(function(e) {
                e.stopPropagation();

                var dropdown = $(this).find('.menuSub');
                var active = dropdown.hasClass('active');

                $('.menuSub').removeClass('active');
                if (!active) dropdown.addClass('active');
        });
        $('#menuHome').click(showHomeScreen);
        $('#menuModal').find('.back').click(function() {
                var modalId = $(this).data('remodal-rel');
                if (modalId) openModal(modalId);
        });
});

// Login
function showLogin() {
        openModal('login');
        $($('#loginUserName').hasValue() ? '#loginUserPassword' : '#loginUserName').focus();
}

function closeLogin() {
        closeModal('login');
}

function checkLogin(callback, showHome, modal) {
<?php if ($loggedIn): ?>
        if (loggedIn) {
                if ($.isFunction(callback)) callback();
        } else {
                $('#login').off('login').on('login', ($.isFunction(callback) ? callback : function() {}));

                showLogin();
        }
<?php else: ?>
        $.localStorage.setItem('home', showHome);
        $.localStorage.setItem('modal', modal);

        showLogin();
<?php endif; ?>
}

function login(e) {
        e.preventDefault();
        $('#loginError').hide();

        var form = $(this);
        form.find('input[type=submit]').attr('disabled', true).end().find('.loading').show();

        loadUrl(form.attr('action'), {
                data: form.serializeObject()
        }, function() {
<?php if ($loggedIn): ?>
                loggedIn = true;
                closeLogin();

                $('#login').trigger('login');
<?php else: ?>
                reloadTopLocation();
<?php endif; ?>
        }, function(err, errShow) {
                loggedIn = false;
                $('#loginError').html(errShow || '<?php _e('An unexpected error has occured.<br>If this error keeps occuring, please contact your vendor for assistance.'); ?>').show();

                logError(err);
        }, function() {
                form.find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
        });
}

function sendPasswordMail(e) {
        e.preventDefault();
        $('#passwordSuccess, #passwordError').hide();

        var form = $(this);
        form.find('input[type=submit]').attr('disabled', true).end().find('.loading').show();

        loadUrl(form.attr('action'), {
                data: form.serializeObject()
        }, function() {
                $('#passwordSuccess').show();
        }, function(err, errShow) {
                $('#passwordError').html(errShow || '<?php _e('An unexpected error has occured.<br>If this error keeps occuring, please contact your vendor for assistance.'); ?>').show();

                logError(err);
        }, function() {
                form.find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
        });
}

<?php if ($loggedIn): ?>
function logoutSession() {
        $('#logoutSessionFrame').attr('src', 'http://<?php echo $user['Name']; ?>.<?php echo VISUALIZATION_DOMAIN; ?>/logout?response=0&time=' + Math.floor(Date.now() / 1000));
}

function logout(e) {
        e.preventDefault();

        var button = $(this);
        if (!button.attr('disabled')) {
                button.attr('disabled', true);

                loadUrl('<?php echo $this->getSessionInvalidateUrl(); ?>', {}, function() {
                        $('#logoutSessionFrame').off('load').on('load', function() {
                               reloadTopLocation();
                        });
                        logoutSession();
                }, null, function() {
                        button.attr('disabled', false);
                });
        }
}

function changeAccountPassword(e) {
        e.preventDefault();
        $('#accountPasswordSuccess, #accountPasswordError').hide();

        var form = $(this);
        form.find('input[type=submit]').attr('disabled', true).end().find('.loading').show();

        loadUrl(form.attr('action'), {
                data: form.serializeObject()
        }, function() {
                $('#accountPasswordSuccess').show();
                form.trigger('reset');
        }, function(err, errShow) {
                $('#accountPasswordError').html(errShow || '<?php _e('An unexpected error has occured.<br>If this error keeps occuring, please contact your vendor for assistance.'); ?>').show();

                logError(err);
        }, function() {
                form.find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
        });
}

function setMapSettings(e) {
        e.preventDefault();
        $('#settingsSuccess, #settingsError').hide();

        var form = $(this);
        form.find('input[type=submit]').attr('disabled', true).end().find('.loading').show();

        loadUrl(form.attr('action'), {
                data: form.serializeObject()
        }, function() {
                $('#settingsSuccess').show();

                visualization.map_privacy = $('#mapPrivacy').val();
                visualization.map_privacy_users = $('#mapUserInput').find('input').val();
                visualization.edit_privacy = $('#editPrivacy').val();
                visualization.edit_privacy_users = $('#editUserInput').find('input').val();
                visualization.edit_proposition = $('#editProposition').is(':checked');
                visualization.edit_proposition_private = !$('#editPropositionPrivate').is(':checked');
        }, function(err, errShow) {
                $('#settingsError').html(errShow || '<?php _e('An unexpected error has occured.<br>If this error keeps occuring, please contact your vendor for assistance.'); ?>').show();

                logError(err);
        }, function() {
                form.find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
        });
}

// Updates
var updateGeometry, updateContainer, oldLayerStyle;

function showMapUpdatesTab() {
        var menuItem = $(this);
        if (!menuItem.hasClass('active')) {
                menuItem.parent().find('> li').removeClass('active');
                menuItem.addClass('active');

                $('#updatesOverview').empty();
                $('#updatesContent').find('.loading').show();

                getUpdates(menuItem.attr('id'), function(data) {
                        var mapUpdatesHTML = '';
                        if (data.total_rows) {
                                mapUpdatesHTML = '<table class="dataTable">\n\
                                                        <thead>\n\
                                                                <tr>\n\
                                                                        <th><h6><?php _e('Name'); ?></h6></th>\n\
                                                                        <th><h6><?php _e('User'); ?></h6></th>\n\
                                                                        <th><h6><?php _e('Status'); ?></h6></th>\n\
                                                                        <th><h6><?php _e('Created at'); ?></h6></th>\n\
                                                                </tr>\n\
                                                        </thead>\n\
                                                        <tbody>';

                                var rows = data.rows;
                                for (var index in rows) {
                                        var row = rows[index];
                                        var creationDate = new Date(row.created_at);

                                        mapUpdatesHTML += '<tr data-rel="' + row.cartodb_id + '">\n\
                                                                <td>' + (row.name === null ? '' : row.name.capitalize()) + '</td>\n\
                                                                <td>' + row.user_name.capitalize() + '</td>\n\
                                                                <td class="' + row.status + '">' + row.status.capitalize() + '</td>\n\
                                                                <td>' + moment(creationDate).format('YYYY-MM-DD HH:mm:ss'); + '</td>\n\
                                                        </tr>';
                                }

                                mapUpdatesHTML += '</tbody>\n\
                                                </table>';
                        } else {
                                mapUpdatesHTML = '<?php _e('There is no available data'); ?>';
                        }
                        $('#updatesOverview').append(mapUpdatesHTML);

                        $('#updatesContent').find('.loading').hide();
                });
        }
}

function getUpdates(menu, callback) {
        var execute = true;

        if (propositionLayer) {
                var query = 'SELECT * FROM ' + propositionLayer.options.table_name + ' AS t';
                var param = null;

                if (menu) {
                        switch (menu) {
                                case 'menuPending':
                                        query += ' WHERE t.status = \'pending\'';
                                        break;
                                case 'menuMy':
                                        query += ' WHERE t.user_name = \'<?php echo $user['Name']; ?>\'';
                                        break;
                                case 'menuResults':
                                        var searchForm = $('#updatesSearchForm');
                                        var text = searchForm.find('input[type=text]');
                                        var search = text.val().toLowerCase();

                                        var whereSet = false;
                                        if (visualization.edit_proposition_private) {
                                                query += ' WHERE t.user_name = \'<?php echo $user['Name']; ?>\'';
                                                whereSet = true;
                                        }

                                        if (search !== '') {
                                                var updateId = false;
                                                var lastDash = search.lastIndexOf('-');
                                                if (lastDash !== -1) {
                                                        var visualizationId = search.substr(0, lastDash);

                                                        if (visualizationId === visualization.<?php echo REQUEST_PARAMETER_VIZ_ID; ?>)
                                                                updateId = search.substr(lastDash + 1);
                                                }

                                                query += (whereSet ? ' AND ' : ' WHERE ') + (updateId ? 't.cartodb_id = {{param}}' : 't::text ILIKE \'%{{param}}%\'');
                                                param = (updateId ? updateId : search);
                                        } else {
                                                execute = false;
                                        }
                                        break;
                        }
                }
                query += ' ORDER BY t.created_at DESC LIMIT 10;';

                if (execute) {
                        sqlObj.execute(query, {
                                param: param
                        }).done(function(data) {
                                if ($.isFunction(callback)) callback(data);
                        });
                }
        } else {
                execute = false;
        }

        if (!execute && $.isFunction(callback)) callback([]);
}

function initializeUpdate() {
        updateContainer = $('#updateContainer');
}

function openUpdate(id, callback) {
        if (propositionLayer) {
                var query = 'SELECT *, ST_AsText(the_geom) AS geom, GeometryType(the_geom) as geom_type, ' +
                            'ST_XMin(ST_Extent(the_geom)) as minx, ST_YMin(ST_Extent(the_geom)) as miny, ST_XMax(ST_Extent(the_geom)) as maxx, ST_YMax(ST_Extent(the_geom)) as maxy ' +
                            'FROM {{table}} WHERE cartodb_id = {{id}} GROUP BY cartodb_id LIMIT 1;';

                sqlObj.execute(query, {
                        table: propositionLayer.options.table_name,
                        id: id
                }).done(function(data) {
                        if (data.total_rows) {
                                var row = data.rows[0];

                                if (visualization.<?php echo REQUEST_PARAMETER_MYMAP ?> || row.user_name === '<?php echo $user['Name']; ?>' || !visualization.edit_proposition_private) {
                                        if (row.feature_action === '<?php echo EDITOR_ACTION_NEW_FEATURE; ?>') {
                                                showUpdate(row);
                                                if ($.isFunction(callback)) callback();
                                        } else {
                                                sqlObj.execute(query, {
                                                        table: mapLayer.options.table_name,
                                                        id: row.feature_id
                                                }).done(function(originalData) {
                                                        if (originalData.total_rows) {
                                                                var originalRow = originalData.rows[0];

                                                                showUpdate(row, originalRow);
                                                                if ($.isFunction(callback)) callback();
                                                        }
                                                });
                                        }
                                }
                        }
                });
        }
}

function showUpdate(row, originalRow) {
        hideUpdate();

        var bounds = null;
        if (row.the_geom) {
                var colors = ['#000000', '#FFFFFF', '#FF00FF', '#FF0000', '#FFFF00', '#00FF00', '#00FFFF', '#0000FF'];
                switch (row.geom_type) {
                        case '<?php echo EDITOR_POINT; ?>':
                                var marker_file = '';
                                if (/marker-file\s*:.+?;/.test(row.feature_style)) {
                                        try {
                                                marker_file = row.feature_style.match(/marker-file\s*:.+?;/)[0].replace(/\/\*line-brake\*\//g, '').match(/http:.+?(svg)/)[0];
                                        } catch (err) {
                                                marker_file = '';
                                        }
                                        if (/http:\/\/images\.spotzi\.com\/mapbuilder\/editor\/icons\/.+?(\.svg)/.test(marker_file) === false)
                                                marker_file = '';
                                }
                                var marker_fill = '';
                                if (/marker-fill\s*:.+?;/.test(row.feature_style)) {
                                        try {
                                                marker_fill = row.feature_style.match(/marker-fill\s*:.+?;/)[0].replace(/\/\*line-brake\*\//g, '').split(':')[1].replace(';', '').trim();
                                        } catch (err) {
                                                marker_fill = '';
                                        }
                                        if ($.inArray(marker_fill, colors) === -1) marker_fill = '';
                                }
                                var coordinates = row.geom.match(/(\-?\d+(\.\d+)?(e\-\d+)?)\s*(\-?\d+(\.\d+)?(e\-\d+)?)/)[0].split(' ');
                                var style_options = { marker_file: marker_file, color: marker_fill };

                                updateGeometry = new L.CircleMarker({ lng: parseFloat(coordinates[0]), lat: parseFloat(coordinates[1]) }, {
                                        radius: 4,
                                        weight: 1,
                                        opacity: 1,
                                        color: '#000000',
                                        fillColor: marker_fill,
                                        fillOpacity: 1,
                                        pointerEvents: 'mousedown'
                                });
                                break;
                        case '<?php echo EDITOR_LINE; ?>':
                                var line_color = '';
                                if (/line-color\s*:.+?;/.test(row.feature_style)) {
                                        try {
                                                line_color = row.feature_style.match(/line-color\s*:.+?;/)[0].replace(/\/\*line-brake\*\//g, '').split(':')[1].replace(';', '').trim();
                                        } catch (err) {
                                                line_color = '';
                                        }
                                        if ($.inArray(line_color, colors) === -1) line_color = '';
                                }

                                var the_geom = [];
                                var lines = row.geom.split('),(');
                                var lineCount = lines.length;
                                for (var lineIndex = 0; lineIndex < lineCount; lineIndex++) {
                                        var coordinates = lines[lineIndex].match(/(\-?\d+(\.\d+)?(e\-\d+)?)\s*(\-?\d+(\.\d+)?(e\-\d+)?)/g);
                                        var tempGeom = [];

                                        var coordinateCount = (coordinates.length);
                                        for (var coordNum = 0; coordNum < coordinateCount; coordNum++) {
                                                tempGeom.push({ lng: parseFloat(coordinates[coordNum].split(' ')[0]), lat: parseFloat(coordinates[coordNum].split(' ')[1]) });
                                        }
                                        the_geom.push(tempGeom);
                                }

                                updateGeometry = new L.MultiPolyline(the_geom, {
                                        color: line_color
                                });
                                break;
                        case '<?php echo EDITOR_POLYGON; ?>':
                                var polygon_fill = '';
                                if (/polygon-fill\s*:.+?;/.test(row.feature_style)) {
                                        try {
                                                polygon_fill = row.feature_style.match(/polygon-fill\s*:.+?;/)[0].replace(/\/\*line-brake\*\//g, '').split(':')[1].replace(';', '').trim();
                                        } catch (err) {
                                                polygon_fill = '';
                                        }
                                        if ($.inArray(polygon_fill, colors) === -1) polygon_fill = '';
                                }

                                var the_geom = [];
                                var polygons = row.geom.split('),(');
                                var polyCount = polygons.length;
                                for (var polyIndex = 0; polyIndex < polyCount; polyIndex++) {
                                        var coordinates = polygons[polyIndex].match(/(\-?\d+(\.\d+)?(e\-\d+)?)\s*(\-?\d+(\.\d+)?(e\-\d+)?)/g);
                                        var tempGeom = [];

                                        var coordinateCount = (coordinates.length - 1);
                                        for (var coordNum = 0; coordNum < coordinateCount; coordNum++) {
                                                tempGeom.push({ lng: parseFloat(coordinates[coordNum].split(' ')[0]), lat: parseFloat(coordinates[coordNum].split(' ')[1]) });
                                        }
                                        the_geom.push(tempGeom);
                                }

                                updateGeometry = new L.MultiPolygon(the_geom, {
                                        weight: 1,
                                        opacity: 1,
                                        color: '#000000',
                                        fillColor: polygon_fill
                                });
                                break;
                }

                bounds = L.latLngBounds([row.miny, row.minx], [row.maxy, row.maxx]);

                if (updateGeometry) {
//                        mapLayers[visualization.<?php echo REQUEST_PARAMETER_VIZ_LAYER_INDEX; ?>].setOpacity(0.5);
                        map.addLayer(updateGeometry);
                }
        } else if (originalRow) {
                bounds = L.latLngBounds([originalRow.miny, originalRow.minx], [originalRow.maxy, originalRow.maxx]);
        }

        if (bounds) {
                map.fitBounds(bounds, {
                        padding: [120, 120]
                });

                var containerHTML = '<div id="updateClose" class="fa-icon fa-times"></div>\n\
                                     <div id="updateContent" class="scroll">\n\
                                        <h4 id="updateGeneral"><?php _e('General information'); ?></h4>';

                // Name, title and user
                if (row.name) containerHTML += '<p><b><?php _e('Update ID'); ?></b><br>' + visualization.<?php echo REQUEST_PARAMETER_VIZ_ID; ?> + '-' + row.cartodb_id + '</p>';
                if (row.description) containerHTML += '<p><b><?php _e('Proposal description'); ?></b><br>' + row.description.capitalize().nl2br() + '</p>';
                if (row.review_description) containerHTML += '<p><b><?php _e('Review description'); ?></b><br>' + row.review_description.capitalize().nl2br() + '</p>';
                containerHTML += '<p><b><?php _e('User'); ?></b><br>' + row.user_name.capitalize() + '</p>';

                // Action types
                var actions = row.feature_action.split(',');

                var actionTypes = [];
                for (var index in actions) {
                        switch (actions[index]) {
                                case '<?php echo EDITOR_ACTION_NEW_FEATURE; ?>':
                                        actionTypes.push('<?php _e('New feature'); ?>');
                                        break;
                                case '<?php echo EDITOR_ACTION_EDIT_DATA; ?>':
                                        actionTypes.push('<?php _e('Data edit'); ?>');
                                        break;
                                case '<?php echo EDITOR_ACTION_EDIT_GEOM; ?>':
                                        actionTypes.push('<?php _e('Geometry edit'); ?>');
                                        break;
                                case '<?php echo EDITOR_ACTION_EDIT_STYLE; ?>':
                                        if (mapLayer && row.feature_id && row.feature_style && row.status === 'pending') {
                                                oldLayerStyle = mapLayer.sub.getCartoCSS();
                                                var layerCSS = mapLayer.sub.getCartoCSS().replace(/\n/g, '/*line-brake*/');
                                                var regex = new RegExp('#\\w+\\s*\\[\\s*cartodb_id\\s*=\\s*' + row.feature_id + '\\s*\\].+?\\}');
                                                if (regex.test(layerCSS)) {
                                                        layerCSS = layerCSS.replace(regex, row.feature_style);
                                                } else {
                                                        layerCSS += row.feature_style;
                                                }
                                                mapLayer.sub.setCartoCSS(layerCSS.replace(/\/\*line-brake\*\//g, '\n'));
                                        }

                                        actionTypes.push('<?php _e('Style edit'); ?>');
                                        break;
                                case '<?php echo EDITOR_ACTION_DELETE; ?>':
                                        actionTypes.push('<?php _e('Feature delete'); ?>');
                                        break;
                        }
                }
                containerHTML += '<p><b><?php _e('Update type'); ?></b><br>' + actionTypes.join(' / ') + '</p>';

                // Creation date
                var creationDate = new Date(row.created_at);
                containerHTML += '<p><b><?php _e('Created at'); ?></b><br>' + moment(creationDate).format('YYYY-MM-DD HH:mm:ss') + '</p>';

                // Share
                var updateUrl = '<?php echo URL_BASE; ?>?<?php echo REQUEST_PARAMETER_UPDATE_ID; ?>=' + visualization.<?php echo REQUEST_PARAMETER_VIZ_ID; ?> + '-' + row.cartodb_id;
                containerHTML += '<p><b><?php _e('Share'); ?></b><br><input class="select" type="text" value="' + updateUrl + '" readonly></p>';

                // Column data
                containerHTML += '<?php _e('<h4 id="updateColumns">Column data</h4>'); ?>';
                var columnData = [];
                try {
                        columnData = $.parseJSON(row.column_data);
                } catch (err) {}
                if (columnData) {
                        for (var column in columnData) {
                                containerHTML += '<p><b>' + column.capitalize() + '</b>' +
                                                 '<br>' + (columnData[column] === null || columnData[column] === '' ? '<i><?php _e('No data'); ?></i>' : columnData[column].nl2br()) + '</p>';
                        }
                } else {
                        containerHTML += '<?php _e('No column data was changed.'); ?>';
                }
                containerHTML += '</div>';

                // Status
                var statusText = '';
                switch (row.status) {
                        case 'pending':
                                statusText = '<?php _e('Pending'); ?>';
                                break;
                        case 'accepted':
                                statusText = '<?php _e('Accepted'); ?>';
                                break;
                        case 'declined':
                                statusText = '<?php _e('Declined'); ?>';
                                break;
                }

                containerHTML += '<div id="updateOptions">\n\
                                        <h4 id="updateStatus" class="clear"><?php _e('Status'); ?><span class="' + row.status + '">' + statusText + '</span></h4>\n\
                                        <p id="updateReview" class="clear"></p>\n\
                                </div>\n\
                                <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>';

                updateContainer.append(containerHTML).show();

                // Review
                if (row.status === 'pending' && visualization.<?php echo REQUEST_PARAMETER_MYMAP; ?>) {
                        reviewHTML = '<input class="reject" type="button" value="<?php _e('Reject'); ?>"><input class="accept" type="button" value="<?php _e('Accept'); ?>">';

                        $('#updateReview').append(reviewHTML).find('.reject').click(function() {
                                $('#updateContent').empty().append('<textarea id="reviewDescription" class="userText" name="proposition_confirm_description" placeholder="<?php _e('Describe the reason for rejecting this proposition'); ?>" cols="40" rows="4"></textarea>');
                                $('#updateReview').find('input.accept').hide();
                                $('#updateReview').find('input.reject').off('click').click( function() {
                                        updateContainer.find('.loading').show();
                                        $('#updateReview').find('input').attr('disabled','disabled');
                                        var reviewDescription = $('#updateContent').find('#reviewDescription').val();
                                        loadUrl('<?php echo $this->getVisualizationPropositionUpdateUrl(); ?>', {
                                                data: {
                                                        propositionId: row.cartodb_id,
                                                        newStatus: 'declined',
                                                        reviewDescription: reviewDescription
                                                }
                                        }, function() {
                                                mapReload(false, function() {
                                                        openUpdate(row.cartodb_id);
                                                });
                                        }, function() {}, function() {
                                                updateContainer.find('.loading').hide();
                                        });
                                });
                        }).end().find('.accept').click(function() {
                                $('#updateContent').empty().append('<textarea id="reviewDescription" class="userText" name="proposition_confirm_description" placeholder="<?php _e('Describe the reason for accepting this proposition'); ?>" cols="40" rows="4"></textarea>');
                                $('#updateReview').find('input.reject').hide();
                                $('#updateReview').find('input.accept').off('click').click( function() {
                                        updateContainer.find('.loading').show();
                                        $('#updateReview').find('input').attr('disabled','disabled');
                                        oldLayerStyle = null;
                                        var reviewDescription = $('#updateContent').find('#reviewDescription').val();
                                        loadUrl('<?php echo $this->getVisualizationPropositionUpdateUrl(); ?>', {
                                                data: {
                                                        propositionId: row.cartodb_id,
                                                        newStatus: 'accepted',
                                                        reviewDescription: reviewDescription
                                                }
                                        }, function() {
                                                mapReload(true, function() {
                                                        openUpdate(row.cartodb_id);
                                                });
                                        }, function() {}, function() {
                                                updateContainer.find('.loading').hide();
                                        });
                                });
                        });
                }

                initializeScrolls(updateContainer);
                closeModal('mapUpdates');

                map.on('move', function() {
                        var placeOnScreen = map.latLngToContainerPoint(bounds.getNorthWest());
                        updateContainer.css({
                                top: placeOnScreen.y,
                                left: (placeOnScreen.x - updateContainer.outerWidth() - 10)
                        });
                }, updateContainer).fire('move', updateContainer);
        }
}

function hideUpdate() {
        updateContainer.empty().hide();
        map.off('move', updateContainer);

        if (updateGeometry) {
                map.removeLayer(updateGeometry);
                updateGeometry = null;
        }

        if (mapLayer && oldLayerStyle) {
                mapLayer.sub.setCartoCSS(oldLayerStyle);
                oldLayerStyle = null;
        }
}
<?php else: ?>
// Register
function showRegister() {
        openModal('register');
        $($('#registerUserName').hasValue() ? ($('#registerUserEmail').hasValue() ? '#registerUserPassword' : '#registerUserEmail') : '#registerUserName').focus();
}

function register(e) {
        e.preventDefault();
        $('#registerError').hide();

        var form = $(this);
        form.find('input[type=submit]').attr('disabled', true).end().find('.loading').show();

        loadUrl(form.attr('action'), {
                data: form.serializeObject()
        }, function() {
                reloadTopLocation(true);
        }, function(err, errShow) {
                $('#registerError').html(errShow || '<?php _e('An unexpected error has occured.<br>If this error keeps occuring, please contact your vendor for assistance.'); ?>').show();

                logError(err);
        }, function() {
                form.find('input[type=submit]').attr('disabled', false).end().find('.loading').hide();
        });
}
<?php endif; ?>

// General
function openModal(id) {
        var modal = $('[data-remodal-id=' + id + ']');
        if (modal.length) modal.remodal({ hashTracking: false }).open();
}

function closeModal(id) {
        var modal = $('[data-remodal-id=' + id + ']');
        if (modal.length && modal.remodal().getState() === 'opened') modal.remodal().close();
}
</script>
<div id="menu">
        <div id="mainMenu">
                <div id="menuButtons">
                        <div id="menuHome" class="menuButton fa-icon fa-bars" title="<?php _e('Home'); ?>"></div>
                </div>
                <div id="menuSearch" class="menuSearch">
                        <form id="menuSearchForm" class="menuSearchForm" method="post" enctype="multipart/form-data" action="#">
                                <div id="menuSearchText" class="menuSearchText">
                                        <input type="text" placeholder="<?php _e('Search for an address or a theme'); ?>" value="">
                                </div>
                                <div id="menuSearchSubmit" class="menuSearchSubmit">
                                        <input type="submit" class="fa-icon" value="&#xf002;">
                                </div>
                        </form>
                </div>
        </div>
        <div id="subMenu">
                <div id="menuButtons">
<?php if ($loggedIn): ?>
                        <div id="menuMapUpdates" class="menuButton fa-icon fa-pencil-square-o" title="<?php _e('Map updates'); ?>">
                                <span class="title"><?php _e('Map updates'); ?></span>
                        </div>
                        <div id="menuMapSettings" class="menuButton fa-icon fa-cog" title="<?php _e('Map settings'); ?>">
                                <span class="title"><?php _e('Map settings'); ?></span>
                        </div>
                        <div id="menuMapEdit" class="menuButton fa-icon fa-pencil" title="<?php _e('Edit'); ?>">
                                <span class="title"><?php _e('Edit'); ?></span>
                                <ul id="editMenu" class="menuSub">
                                        <li id="addPointButton" class="fa-icon fa-circle drawFeatureButton" title="<?php _e('Add point'); ?>"><?php _e('Add point'); ?></li>
                                        <li id="addLineButton" class="borderTop fa-icon fa-minus drawFeatureButton" title="<?php _e('Add line'); ?>"><?php _e('Add line'); ?></li>
                                        <li id="addPolygonButton" class="borderTop fa-icon fa-delicious drawFeatureButton" title="<?php _e('Add polygon'); ?>"><?php _e('Add polygon'); ?></li>
                                        <li id="mapEdit" class="borderTop fa-icon fa-pencil-square-o" title="<?php _e('Advanced editor'); ?>"><?php _e('Advanced editor'); ?></li>
                                </ul>
                        </div>
<?php endif; ?>
                        <div id="menuAccount" class="<?php if ($loggedIn): ?>menuUser <?php endif; ?>menuButton fa-icon fa-user">
                                <ul class="menuSub">
<?php if ($loggedIn): ?>
                                        <li id="menuUser" class="menuUser fa-icon fa-user" title="<?php _e('Account'); ?>"><?php echo ucwords(isset($user['FullName']) ? $user['FullName'] : $user['Name']); ?></li>
                                        <li id="menuMaps" class="borderTop fa-icon fa-globe" title="<?php _e('My Maps'); ?>"><?php _e('My Maps'); ?></li>
                                        <li id="menuLogout" class="borderTop fa-icon fa-sign-out" class="borderTop" title="<?php _e('Log out'); ?>"><?php _e('Log out'); ?></li>
<?php else: ?>
                                        <li id="menuUser" class="menuNoUser fa-icon fa-user" title="<?php _e('Log in'); ?>"><?php _e('Not logged in'); ?></li>
                                        <li id="menuLogin" class="fa-icon fa-sign-in" title="<?php _e('Log in'); ?>"><?php _e('Log in'); ?></li>
                                        <li id="menuRegister" class="borderTop fa-icon fa-user-plus" class="borderTop" title="<?php _e('Register'); ?>"><?php _e('Register'); ?></li>
<?php endif; ?>
                                </ul>
                        </div>
                </div>
        </div>
        <div id="menuModal">
                <div id="login" data-remodal-id="login">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('Log in'); ?></h4>
                        <div id="loginUser">
                                <p class="description"><?php _e('In order to gain access to this function you must log in to your account.'); ?></p>
                                <form id="loginForm" class="clear" method="post" enctype="multipart/form-data" action="<?php echo $this->getSessionValidateUrl(); ?>">
                                        <p id="loginError" class="spotziRed" style="display: none;"></p>
                                        <p><input id="loginUserName" type="text" name="<?php echo REQUEST_PARAMETER_USER_NAME; ?>" placeholder="<?php _e('User name or email address'); ?>" required></p>
                                        <p><input id="loginUserPassword" type="password" name="<?php echo REQUEST_PARAMETER_USER_PASSWORD; ?>" placeholder="<?php _e('Password'); ?>" required></p>
                                        <p class="note clear">
<?php if (!$loggedIn): ?>
                                                <span><?php _e('Don\'t have an account? <a href="javascript:void(0);" onclick="showRegister();">Click here</a> to create one.'); ?></span>
<?php endif; ?>
                                                <input id="loginSubmit" type="submit" value="<?php _e('Log in'); ?>">
                                        </p>
                                        <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                                </form>
                        </div>
                </div>
<?php if ($loggedIn): ?>
                <div id="logoutSession">
                        <iframe id="logoutSessionFrame" style="display: none;"></iframe>
                </div>
                <div id="account" data-remodal-id="account">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('My account'); ?></h4>
                        <div id="accountProfile">
                                <h5><?php _e('Profile'); ?></h5>
                                <table id="profileTable">
                                        <tr>
                                                <td><?php _e('User name'); ?></td>
                                                <td><?php echo ucwords(isset($user['FullName']) ? $user['FullName'] : $user['Name']); ?></td>
                                        </tr>
                                        <tr>
                                                <td><?php _e('Email address'); ?></td>
                                                <td><?php echo $user['Email']; ?></td>
                                        </tr>
<?php   if (isset($user['AccountType'])): ?>
                                        <tr>
                                                <td><?php _e('Account type'); ?></td>
                                                <td><?php echo ucfirst($user['AccountType']); ?></td>
                                        </tr>
<?php   endif; ?>
                                </table>
                        </div>
                        <div id="accountPassword">
                                <h5><?php _e('Change password'); ?></h5>
                                <form id="accountPasswordForm" method="post" enctype="multipart/form-data" action="<?php echo $this->getSessionUserUrl(); ?>">
                                        <p id="accountPasswordSuccess" class="spotziGreen" style="display: none;"><?php _e('Your password was successfully changed'); ?></p>
                                        <p id="accountPasswordError" class="spotziRed" style="display: none;"></p>
                                        <p><input id="accountPasswordOld" type="password" name="passwordOld" placeholder="<?php _e('Current password'); ?>" required></p>
                                        <p><input id="accountPasswordNew" type="password" name="passwordNew" placeholder="<?php _e('New password'); ?>" required></p>
                                        <p><input id="accountPasswordConfirm" type="password" name="passwordConfirm" placeholder="<?php _e('Confirm password'); ?>" required></p>
                                        <p class="clear"><input id="accountPasswordSubmit" type="submit" value="<?php _e('Change password'); ?>"></p>
                                        <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                                </form>
                        </div>
                </div>
                <div id="mapUpdates" data-remodal-id="mapUpdates">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('Map updates'); ?></h4>
                        <div id="updatesContent">
                                <div id="updatesSearch">
                                        <form id="updatesSearchForm" method="post" enctype="multipart/form-data" action="#">
                                                <input type="text" placeholder="<?php _e('Search updates'); ?>" value="">
                                        </form>
                                </div>
                                <p>
                                        <ul id="updatesMenu" class="clear">
                                                <li id="menuRecent"><h5><?php _e('Recent'); ?></h5></li>
                                                <li id="menuPending"><h5><?php _e('Pending'); ?></h5></li>
                                                <li id="menuMy"><h5><?php _e('My updates'); ?></h5></li>
                                                <li id="menuResults"><h5><?php _e('Search'); ?></h5></li>
                                        </ul>
                                </p>
                                <div id="updatesOverview"></div>
                                <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                        </div>
                </div>
                <div id="mapSettings" data-remodal-id="mapSettings">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('Map settings'); ?></h4>
                        <div id="settingsContent">
                                <form id="settingsForm" method="post" enctype="multipart/form-data" action="<?php echo $this->getVisualizationUpdateUrl(); ?>">
                                        <p id="settingsSuccess" class="spotziGreen" style="display: none;"><?php _e('The settings were saved successfully.'); ?></p>
                                        <p id="settingsError" class="spotziRed" style="display: none;"><?php _e('The settings could not be saved.'); ?></p>
                                        <div id="mapPrivacyContent">
                                                <h5><?php _e('View privacy'); ?></h5>
                                                <p>
                                                        <select id="mapPrivacy" name="mapPrivacy">
                                                                <option value="public"><?php _e('Public'); ?></option>
                                                                <option value="link"><?php _e('Restricted'); ?></option>
                                                                <option value="private"><?php _e('Private'); ?></option>
                                                        </select>
                                                        <div id="mapUserInput" class="userInput">
                                                                <input type="text" name="mapPrivacyUsers" placeholder="<?php _e('Allowed users'); ?>" value="">
                                                                <p class="note"><?php _e('Comma separated list of user names'); ?></p>
                                                        </div>
                                                </p>
                                        </div>
                                        <div id="editPrivacyContent">
                                                <h5><?php _e('Edit privacy'); ?></h5>
                                                <p>
                                                        <select id="editPrivacy" name="editPrivacy">
                                                                <option value="public"><?php _e('Public'); ?></option>
                                                                <option value="link"><?php _e('Restricted'); ?></option>
                                                                <option value="private"><?php _e('Private'); ?></option>
                                                        </select>
                                                        <div id="editUserInput" class="userInput">
                                                                <input type="text" name="editPrivacyUsers" placeholder="<?php _e('Allowed users'); ?>" value="">
                                                                <p class="note"><?php _e('Comma separated list of user names'); ?></p>
                                                        </div>
                                                </p>
                                        </div>
                                        <div id="editPropositionContent">
                                                <h5><?php _e('Edit mode'); ?></h5>
                                                <p>
                                                        <label for="editProposition" class="checkbox">
                                                                <input id="editProposition" type="checkbox" name="editProposition">
                                                                <span></span>
                                                                <?php _e('Review community updates'); ?>
                                                        </label>
                                                </p>
                                                <p>
                                                        <label for="editPropositionPrivate" class="checkbox">
                                                                <input id="editPropositionPrivate" type="checkbox" name="editPropositionPrivate">
                                                                <span></span>
                                                                <?php _e('Community updates are viewable by peers'); ?>
                                                        </label>
                                                </p>
                                        </div>
                                        <p class="note clear">
                                                <input id="doSettings" type="submit" value="<?php _e('Update settings'); ?>">
                                        </p>
                                        <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                                </form>
                        </div>
                </div>
<?php else: ?>
                <div id="register" data-remodal-id="register">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h4><?php _e('Register'); ?></h4>
                        <div id="registerContent">
                                <p class="description"><?php _e('In order to gain access to this function you must create an account.'); ?></p>
                                <form id="registerForm" method="post" enctype="multipart/form-data" action="<?php echo $this->getSessionRegisterUrl(); ?>">
                                        <p id="registerError" class="spotziRed" style="display: none;"></p>
                                        <p><input id="registerUserName" type="text" name="<?php echo REQUEST_PARAMETER_USER_NAME; ?>" placeholder="<?php _e('User name'); ?>" required></p>
                                        <p><input id="registerUserEmail" type="email" name="<?php echo REQUEST_PARAMETER_USER_EMAIL; ?>" placeholder="<?php _e('Email address'); ?>" required></p>
                                        <p><input id="registerUserPassword" type="password" name="<?php echo REQUEST_PARAMETER_USER_PASSWORD; ?>" placeholder="<?php _e('Password'); ?>" required></p>
                                        <p class="note clear">
                                                <span><?php _e('Already have an account? <a href="javascript:void(0);" onclick="showLogin();">Click here</a> to log in.'); ?></span>
                                                <input id="doRegister" type="submit" value="<?php _e('Register'); ?>">
                                        </p>
                                        <div class="loading"><div class="fa-icon fa-spinner fa-pulse"></div></div>
                                </form>
                                <p class="note"><?php _e('By registering you are accepting the <a href="http://www.spotzi.com/en/terms-of-service/" target="_blank">terms and conditions</a> of the service and the <a href="http://www.spotzi.com/en/privacy/" target="_blank">privacy policy</a>.'); ?></p>
                        </div>
                </div>
<?php endif; ?>
        </div>
</div>
<?php
if ($loggedIn) require_once('edit.php');
?>
<div id="updateContainer"></div>