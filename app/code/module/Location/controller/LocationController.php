<?php
/**
 * Class LocationController - Location controller.
 *
 * @category    Spotzi Visualization
 * @package     Module
 * @subpackage  Location
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class LocationController extends ModuleController {
        public function search() {
                $this->render();
        }
}