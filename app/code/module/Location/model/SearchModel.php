<?php
/**
 * Class SearchModel - Location search model.
 *
 * @category    Spotzi Visualization
 * @package     Module
 * @subpackage  Location
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class SearchModel extends ModuleModel {
        public $query                                   = null;
        public $bbox                                    = null;

        public function validateRequestParams() {
                $this->bbox = $this->getParam(REQUEST_PARAMETER_BOUNDINGBOX);

                $this->query = $this->getParam(REQUEST_PARAMETER_QUERY);
                if (!$this->query) ErrorHandler::error(E_ERROR, "Missing parameter '%s'", REQUEST_PARAMETER_QUERY);
        }

        public function searchLocation() {
                $webserviceUrl = Text::prepare('%sgeocoding/wo/search?user=%s&password=%s&value=%s&bbox=%s&limit=%s&includeAddress=0&acceptLanguage=%s&format=application/json',
                                               WEBSERVICE_URL, WEBSERVICE_USER, WEBSERVICE_PASSWORD, urlencode($this->query),
                                               urlencode($this->bbox), VISUALIZATION_SEARCH_LIMIT, strtolower(substr($this->request->locale, 0, 2)));

                $location = array();
                $requestContents = Connectivity::runCurl($webserviceUrl);
                if ($requestContents) {
                        $jsonOutput = json_decode($requestContents, true);

                        if (isset($jsonOutput['response']['search'])) {
                                $locationOutput = $jsonOutput['response']['search'];

                                foreach ($locationOutput as $locationEntry) {
                                        $locationResult = array();

                                        $locationResult[REQUEST_PARAMETER_LOCATION_NAME] = $locationEntry['display_name'];
                                        $locationResult[REQUEST_PARAMETER_X] = $locationEntry['lon'];
                                        $locationResult[REQUEST_PARAMETER_Y] = $locationEntry['lat'];

                                        $location[] = $locationResult;
                                }
                        }
                }

                if ($location === false) ErrorHandler::error(E_NOTICE, 'An error occured while searching location data, webservice URL: %s, webservice response: %s',
                                                             $webserviceUrl, '<pre>' . print_r($requestContents, true) . '</pre>');

                return array(REQUEST_RESULT     => $location);
        }
}