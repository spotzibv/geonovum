<?php
/**
 * Class File - offers functionality related to files.
 *
 * @category    Spotzi Dashboard
 * @package     Core
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class File {
        /**
         * Determine whether the given file exists.
         *
         * @param       string          $fileName       File name
         * @return      boolean                         True when file exists, false otherwise
         */
        public static function exists($fileName) {
                return file_exists($fileName);
        }

        /**
         * Read data from a file.
         *
         * @param       string          $fileName       File name
         * @return      mixed                           File data on success, false otherwise
         */
        public static function read($fileName) {
                if (!$fileName || !self::exists($fileName)) return false;

                return file_get_contents($fileName);
        }

        /**
         * Write data to a file.
         *
         * @param       string          $fileName       File name
         * @param       mixed           $data           Data
         * @return      mixed                           Written bytes on success, false otherwise
         */
        public static function write($fileName, $data) {
                if (!$fileName) return false;

                return file_put_contents($fileName, $data);
        }

        /**
         * Delete a file.
         *
         * @param       string          $fileName       File name
         * @return      boolean                         True on success, false otherwise
         */
        public static function delete($fileName) {
                if (!$fileName) return false;

                return (self::exists($fileName) ? unlink($fileName) : true);
        }

        /**
         * Copy a file.
         *
         * @param       string          $source         Source file name
         * @param       string          $destination    Destination file name
         * @return      boolean                         True on success, false otherwise
         */
        public static function copy($source, $destination) {
                if (!$source || !$destination) return false;

                return (self::exists($source) && copy($source, $destination));
        }

        /**
         * Rename or move a file.
         *
         * @param       string          $source         Source file name
         * @param       string          $destination    Destination file name
         * @return      boolean                         True on success, false otherwise
         */
        public static function rename($source, $destination) {
                if (!$source || !$destination) return false;

                return (self::exists($source) && rename($source, $destination));
        }

        /**
         * Inspect a file.
         *
         * @param       string          $fileName               File name
         * @param       array           $allowedTypes           List containing allowed file types (optional)
         * @param       int             $sizeLimit              File size limit in bytes (optional)
         */
        public static function inspect($fileName, $allowedTypes = array(), $sizeLimit = 524288) {
                if ($fileName && File::exists($fileName)) {
                        // Check on allowed file types when needed
                        if (!empty($allowedTypes)) {
                                $fileInfo = pathinfo($fileName);
                                if (empty($fileInfo['extension']) || !in_array($fileInfo['extension'], $allowedTypes)) return false;
                        }

                        // Check on file size limit when needed
                        if ($sizeLimit > 0 && filesize($fileName) > $sizeLimit) return false;

                        return true;
                }

                return false;
        }

        /**
         * Send a file download to the client.
         *
         * @param       string          $file           File path or content
         * @param       string          $fileName       Desired file name
         * @param       boolean         $isPath         True when downloading an existing file, false otherwise (optional)
         * @param       string          $contentType    Content type (optional)
         */
        public static function sendDownload($file, $fileName, $isPath = true, $contentType = null) {
                // Set the download headers
                header('Pragma: public');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: public');
                header('Content-Description: File Transfer');
                header('Content-Type: ' . ($contentType ? $contentType : 'application/octet-stream'));
                header('Content-Disposition: attachment; filename=' . $fileName);
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: ' . ($isPath ? filesize($file) : strlen($file)));

                // Send the file download
                if ($isPath) {
                        readfile($file);
                        exit();
                } else {
                        exit($file);
                }
        }

        /**
         * Handle a file uploaded by the client.
         *
         * @param       string          $targetDirectory        Target directory
         * @param       string          $parameterName          Parameter name as used in the form element
         * @param       string          $fileName               Target file name (optional)
         * @param       array           $allowedTypes           List containing allowed file types (optional)
         * @param       int             $sizeLimit              File size limit in bytes (optional)
         * @return      boolean                                 True on success, false otherwise
         */
        public static function handleUpload($targetDirectory, $parameterName, $fileName = null, $allowedTypes = array(), $sizeLimit = 524288) {
                if (!Text::endsWith($targetDirectory, DIR_SEPARATOR)) $targetDirectory .= DIR_SEPARATOR;

                // Prepare the file upload list
                $files = $_FILES;
                // Check whether the file upload list contains values and make sure the destination directory exists
                if (empty($files) || (!file_exists($targetDirectory) &&
                    !mkdir($targetDirectory, 0777, true))) return false;

                // PHP changes the $_FILES array format when values in HTML forms are passed as array values
                // (e.g. variable[key]), so "restore" the array format when needed
                foreach ($files as $index => $fileCollection) {
                        if (is_array($files[$index]['name']))
                                $files[$index] = Collection::diverse($fileCollection);
                }

                // Retrieve the correct file upload using the parameter name (e.g. key or variable[key])
                $bracketStartPos = strpos($parameterName, '[');
                if ($bracketStartPos) {
                        $bracketEndPos = strpos($parameterName, ']');
                        $parameterCollection = substr($parameterName, 0, $bracketStartPos);
                        $parameterName = substr($parameterName, ($bracketStartPos + 1), ($bracketEndPos - $bracketStartPos - 1));

                        $file = (isset($files[$parameterCollection][$parameterName]) ? $files[$parameterCollection][$parameterName] : false);
                } else {
                        $file = (isset($files[$parameterName]) ? $files[$parameterName] : false);
                }

                // Validate the file upload
                if (!$file || $file['error'] !== UPLOAD_ERR_OK ||
                    ($sizeLimit > 0 && $file['size'] > $sizeLimit)) return false;

                if (!empty($allowedTypes)) {
                        $fileInfo = pathinfo($file['name']);
                        if (empty($fileInfo['extension']) || !in_array($fileInfo['extension'], $allowedTypes)) return false;
                }

                if (!$fileName) $fileName = basename($file['name']);

                // Return the file move result
                return (move_uploaded_file($file['tmp_name'], $targetDirectory . $fileName) ? $fileName : false);
        }
}