<?php
/**
 * Class Session - holds data from the current user session.
 * Security and session fixation implementation inspired by
 * http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
 *
 * @category    Geonovum
 * @package     Core
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class Session {
        /**
         * Initialize the session.
         *
         * This function, and every other function that writes session data, starts
         * and closes the session. After closing, retrieving data is still possible.
         * This is needed instead of a single start to avoid session blocks across requests.
         */
        public static function initialize() {
                // Set the P3P header (IE session fix, IE8 might also need the X-UA-Compatible IE7=EmulateIE7 header)
//                header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM"');

                // Set the session name (required for IE and Edge)
                session_name(SESSION_NAME);
                // Set the cookie parameters
//                session_set_cookie_params(0, '/', (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : ''));

                // Start the session using the session parameter when needed
                if (isset($_REQUEST[REQUEST_PARAMETER_SESSION_ID]) &&
                    $_REQUEST[REQUEST_PARAMETER_SESSION_ID])
                        session_id($_REQUEST[REQUEST_PARAMETER_SESSION_ID]);

                // Start the session
                self::start();

                // Check whether the current session has expired
                $expires = self::getData('s_expires');
                if ((self::getData('s_obsolete') && !$expires) || ($expires && $expires < time())) {
                        self::reset();
                } else if (!self::getData('s_active')) {
                        self::setData('s_active', true);

                        // Regenerate the current session ID on new sessions and application regeneration requests
                        self::regenerateId();
                }

                // Close the session
                self::close();
        }

        /**
         * Start or resume a session.
         */
        public static function start() {
                if (session_status() === PHP_SESSION_NONE) session_start();
        }

        /**
         * Close the current session.
         */
        public static function close() {
                session_write_close();
        }

        /**
         * Destroy the current session.
         */
        public static function destroy() {
                self::start();
                session_destroy();
        }

        /**
         * Destroy the current session and start a new one.
         */
        public static function reset() {
                self::destroy();
                self::start();
        }

        /**
         * Retrieve the current session ID.
         *
         * @return      string                          Current session ID
         */
        public static function id() {
                return session_id();
        }

        /**
         * Regenerate the current session ID.
         *
         * @param       boolean         $deleteOld      True to delete the old session (optional)
         */
        public static function regenerateId($deleteOld = true, $expires = 10) {
                // Resume the session
                self::start();

                // Regenerate the active session ID
                if (session_status() === PHP_SESSION_ACTIVE && !self::getData('s_obsolete')) {
                        $expiresSec = intval($expires);
                        if (!$deleteOld && $expiresSec) {
                                self::setData('s_obsolete', true);
                                self::setData('s_expires', (time() + $expiresSec));

                                // Resume the session
                                self::start();
                        }

                        // Regenerate the session ID
                        session_regenerate_id($deleteOld);

                        // Retrieve the new session ID
                        $newSession = self::id();
                        // Close the old session
                        self::close();

                        // Start the session using the new session ID
                        session_id($newSession);
                        self::start();

                        if (!$deleteOld && $expiresSec) {
                                self::clearData('s_obsolete');
                                self::clearData('s_expires');
                        }
                }

                // Close the session
                self::close();
        }

        /**
         * Retrieve the current session name.
         *
         * @return      string                          Current session name
         */
        public static function name() {
                return session_name();
        }

        /**
         * Check session data presence.
         *
         * @param       mixed           $key            Key to check (optional)
         * @return      mixed                           True when present, false otherwise
         */
        public static function hasData($key = null) {
                return (is_null($key) ? !empty($_SESSION) : isset($_SESSION[$key]));
        }

        /**
         * Retrieve session data.
         *
         * @param       mixed           $key            When set, the requested key value is returned (optional)
         * @return      mixed                           Data when present, false otherwise
         */
        public static function getData($key = null) {
                // Return all data when no key is set
                if (is_null($key)) return $_SESSION;

                // Return empty if the requested key is not present
                if (!isset($_SESSION[$key])) return null;

                // Return key data
                return $_SESSION[$key];
        }

        /**
         * Set session data.
         *
         * @param       mixed           $key            Data key
         * @param       mixed           $value          Data value
         * @return      mixed                           Data
         */
        public static function setData($key, $value) {
                // Resume the session
                self::start();

                // Link data to the given key
                $_SESSION[$key] = $value;

                // Close the session
                self::close();

                // Return data
                return $value;
        }

        /**
         * Add session data.
         *
         * @param       mixed           $key            Data key
         * @param       mixed           $value          Data value
         * @return      mixed                           Data
         */
        public static function addData($key, $value) {
                // Resume the session
                self::start();

                // Add data to the given key
                $_SESSION[$key][] = $value;

                // Close the session
                self::close();

                // Return data
                return $value;
        }

        /**
         * Clear session data.
         *
         * @param       mixed           $key            Key to clear data from (optional)
         */
        public static function clearData($key = null) {
                // Resume the session
                self::start();

                if (is_null($key)) {
                        // Empty session data
                        $_SESSION = array();
                } elseif (isset($_SESSION[$key])) {
                        // Remove the given key from session data
                        unset($_SESSION[$key]);
                }

                // Close the session
                self::close();
        }

        /**
         * Check session messages presence.
         *
         * @return      mixed                           True when present, false otherwise
         */
        public static function hasMessages() {
                return !empty($_SESSION['messages']);
        }

        /**
         * Retrieve session messages.
         *
         * @param       mixed           $type           Type to retrieve messages for (optional)
         * @return      mixed                           Messages when present, false otherwise
         */
        public static function getMessages($type = null) {
                // Return all messages when no type is set
                if (is_null($type) && isset($_SESSION['messages'])) return $_SESSION['messages'];

                // Return false if the requested type is not present
                if (!isset($_SESSION['messages'][$type])) return array();

                // Return type messages
                return $_SESSION['messages'][$type];
        }

        /**
         * Add session message.
         *
         * @param       mixed           $type           Message type
         * @param       mixed           $value          Message
         */
        public static function addMessage($type, $value) {
                // Resume the session
                self::start();

                // Create type if the requested type is not present
                if (!isset($_SESSION['messages'][$type])) $_SESSION['messages'][$type] = array();

                // Add message to the given type
                $_SESSION['messages'][$type][] = $value;

                // Close the session
                self::close();

                // Return message
                return $value;
        }

        /**
         * Clear session messages.
         *
         * @param       mixed           $type           Type to clear messages for (optional)
         */
        public static function clearMessages($type = null) {
                // Resume the session
                self::start();

                if (is_null($type)) {
                        // Empty session messages
                        $_SESSION['messages'] = array();
                } elseif (isset($_SESSION['messages'][$type])) {
                        // Remove the given type from session messages
                        unset($_SESSION['messages'][$type]);
                }

                // Close the session
                self::close();
        }
}

Session::initialize();