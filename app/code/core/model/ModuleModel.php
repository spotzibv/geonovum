<?php
/**
 * Class ModuleModel - module model implementation.
 *
 * @category    Geonovum
 * @package     Core
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
abstract class ModuleModel extends AbstractModel {
        // Database settings
        // CartoDB read connection specifics
        const DB_CONNECTION_CARTO_READ          = 'CartoDBRead';
        const DB_CONNECTION_CARTO_READ_USER     = 'postgres';
        const DB_CONNECTION_CARTO_READ_PASS     = 'postgres';
        // CartoDB connection basics
        const DB_CONNECTION_CARTO_HOST          = '192.168.10.31';
        const DB_CONNECTION_CARTO_DB            = 'carto_db_development';

        // General
        const VISUALIZATION_JSON_URL            = 'http://%s.spotzi.me/api/v2/viz/%s/viz.json';

        // Visualization JSON variable
        protected $vizJSON                      = array();

        /**
         * Constructor.
         *
         * @param       Request         $request        Request object
         */
        public function __construct(&$request) {
                // Construct the parent class
                parent::__construct($request);

                // Validate the request parameters
                $this->validateRequestParams();
        }

        public abstract function validateRequestParams();

        protected function getVisualization() {
                $loggedIn = Session::getData(REQUEST_PARAMETER_LOGGEDIN);
                $vizUrl = $this->getParam(REQUEST_PARAMETER_VIZ_URL);

                $inspectResult = array(REQUEST_PARAMETER_MYMAP          => false,
                                       'data_layer'                     => false,
                                       'proposition_layer'              => false,
                                       'map_privacy'                    => 'private',
                                       'map_privacy_users'              => '',
                                       'map_enabled'                    => false,
                                       'edit_privacy'                   => 'private',
                                       'edit_privacy_users'             => '',
                                       'edit_enabled'                   => false,
                                       'edit_proposition'               => false,
                                       'edit_proposition_private'       => true);
                if ($vizUrl) {
                        $sessionUser = Session::getData(REQUEST_PARAMETER_USER_NAME);

                        $inspectResult[REQUEST_PARAMETER_VIZ_URL] = $vizUrl;
                        $this->analyseVisualizationUrl($vizUrl, $inspectResult);

                        if ($loggedIn && $vizUrl !== VISUALIZATION_DEFAULT &&
                            $inspectResult[REQUEST_PARAMETER_USER_NAME] === $sessionUser['Name'])
                                $inspectResult[REQUEST_PARAMETER_MYMAP] = true;

                        $vizJSON = $this->getVisualizationJSON();
                        if ($vizJSON) {
                                $dataLayer = end($vizJSON['layers']);

                                if (isset($dataLayer['type']) && $dataLayer['type'] === 'layergroup') {
                                        $layergroupOptions = $dataLayer['options'];

                                        $subLayerStart = (count($layergroupOptions['layer_definition']['layers']) - 1);
                                        $dataLayerSet = false;
                                        for ($i = $subLayerStart; $i >= 0; $i--) {
                                                $subLayer = $layergroupOptions['layer_definition']['layers'][$i];
                                                $table = substr($subLayer['options']['sql'], (stripos($subLayer['options']['sql'], ' from ') + 6));
                                                if (strpos($table, ' ') !== false) $table = substr($table, 0, strpos($table, ' '));

                                                $isPropositionLayer = (preg_match('/_(propose)_\d{3,8}_?\d{3,6}$/i', $table) === 1);
                                                if (!$dataLayerSet && !$isPropositionLayer) {
                                                        $isEditorLayer = (preg_match('/_(point|line|polygon)_\d{3,8}_?\d{3,6}$/i', $table) === 1);
                                                        if (!$isEditorLayer || !$inspectResult['data_layer']) $inspectResult['data_layer'] = $subLayer['id'];
                                                        if (!$isEditorLayer) $dataLayerSet = true;
                                                }

                                                if (!$inspectResult['proposition_layer'] && $isPropositionLayer)
                                                        $inspectResult['proposition_layer'] = $subLayer['id'];

                                                if ($dataLayerSet && $inspectResult['proposition_layer']) break;
                                        }
                                }

                                if (isset($vizJSON['map_options']) && $vizJSON['map_options']) {
                                        $mapOptions = json_decode($vizJSON['map_options'], true);

                                        if (isset($mapOptions['map_owner'])) {
                                                if ($mapOptions['map_privacy'] !== 'private') {
                                                        $restrictedMapUsers = explode(',', $mapOptions['map_privacy_users']);
                                                        array_walk($restrictedMapUsers, function(&$value) {
                                                                $value = trim($value);
                                                        });

                                                        $mapOptions['map_enabled'] = ($inspectResult[REQUEST_PARAMETER_MYMAP] || $mapOptions['map_privacy'] === 'public' ||
                                                                                      in_array($sessionUser['Name'], $restrictedMapUsers));
                                                }
                                                if ($mapOptions['edit_privacy'] !== 'private') {
                                                        $restrictedEditUsers = explode(',', $mapOptions['edit_privacy_users']);
                                                        array_walk($restrictedEditUsers, function(&$value) {
                                                                $value = trim($value);
                                                        });

                                                        $mapOptions['edit_enabled'] = ($inspectResult[REQUEST_PARAMETER_MYMAP] || $mapOptions['edit_privacy'] === 'public' ||
                                                                                       in_array($sessionUser['Name'], $restrictedEditUsers));
                                                }
                                                if ($inspectResult[REQUEST_PARAMETER_MYMAP]) $mapOptions['edit_proposition_private'] = false;

                                                $inspectResult = array_merge($inspectResult, $mapOptions);
                                        }
                                } elseif ($inspectResult[REQUEST_PARAMETER_MYMAP]) {
                                        $inspectResult['map_enabled'] = $inspectResult['edit_enabled'] = true;
                                } else {
                                        // @todo: use visualization privacy setting
                                }
                        }
                }

                return $inspectResult;
        }

        protected function analyseVisualizationUrl($vizUrl, &$result) {
                $user = null;
                $userPosStart = strpos($vizUrl, '://');
                if ($userPosStart !== false) {
                        $userPosStart += 3;
                        $userPosEnd = strpos($vizUrl, '.', $userPosStart);

                        if ($userPosEnd !== false) $user = substr($vizUrl, $userPosStart, ($userPosEnd - $userPosStart));
                }
                $result[REQUEST_PARAMETER_USER_NAME] = $user;

                $vizId = null;
                $vizIdPosStart = stripos($vizUrl, '/viz/');
                if ($vizIdPosStart !== false) {
                        $vizIdPosStart += 5;
                        $vizIdPosEnd = strpos($vizUrl, '/', $vizIdPosStart);

                        if ($vizIdPosEnd !== false) $vizId = substr($vizUrl, $vizIdPosStart, ($vizIdPosEnd - $vizIdPosStart));
                }
                $result[REQUEST_PARAMETER_VIZ_ID] = $vizId;
        }

        protected function getVisualizationJSON($vizUrl = null) {
                if (!$vizUrl) $vizUrl = $this->getParam(REQUEST_PARAMETER_VIZ_URL);
                if ($vizUrl) {
                        $this->analyseVisualizationUrl($vizUrl, $analyseResult);

                        if (isset($analyseResult[REQUEST_PARAMETER_VIZ_ID]) && (!$this->vizJSON || $analyseResult[REQUEST_PARAMETER_VIZ_ID] !== $this->vizJSON['id'])) {
                                $this->vizJSON = array();
                                $vizJSONResult = Connectivity::runCurl($vizUrl);
                                if ($vizJSONResult) {
                                        $vizJSON = json_decode($vizJSONResult, true);

                                        if (isset($vizJSON['id'])) $this->vizJSON = $vizJSON;
                                }
                        }
                }

                return $this->vizJSON;
        }

        protected function getVisualizationUser($userName) {
                $db = PostgreSqlDatabase::getInstance();
                // Connect to the Visualization database as a read-only user and as a write user
                $db->connect(self::DB_CONNECTION_CARTO_READ, self::DB_CONNECTION_CARTO_HOST, self::DB_CONNECTION_CARTO_READ_USER,
                             self::DB_CONNECTION_CARTO_READ_PASS, self::DB_CONNECTION_CARTO_DB);

                $user = array();
                $userSelect = $db->select(self::DB_CONNECTION_CARTO_READ, 'users',
                                          AbstractDatabase::SELECT_FIELDS_ALL,
                                          'username=?', array($userName));
                if ($userSelect) $user = reset($userSelect);

                return $user;
        }
}