<?php
/**
 * Class CoreModel - offers core functions.
 *
 * @category    Geonovum
 * @package     Core
 * @author      Ruben Woudenberg <ruben@spotzi.com>
 */
class CoreModel extends AbstractModel {
        /**
         * Validate a request.
         *
         * @return      boolean                         True when valid, false otherwise
         */
        public function validateRequest() {
                // In case of a missing request, throw an exception
                if (empty($this->request)) ErrorHandler::error(E_ERROR, 'No request object was found for validation');

                // Validate the endpoint
                $service = $this->validateEndpoint();
                // In case of an invalid endpoint, throw an exception
                if (!$service) ErrorHandler::error(E_ERROR, 'Invalid endpoint specified');

                if (Session::getData(REQUEST_PARAMETER_LOGGEDIN)) {
                        $user = Session::getData(REQUEST_PARAMETER_USER_NAME);

                        // Clear the session when needed
                        if (!isset($user['Name'])) $this->clearSession();

                        $this->setParam('freshLogin', (boolean) Session::getData('freshLogin'));
                        Session::clearData('freshLogin');
                }

                // Set the locale
//                $locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
                $this->setLocale(REQUEST_LOCALE_DEFAULT);

                // Return the validation result
                return true;
        }

        /**
         * Validate a requested endpoint.
         *
         * @return      mixed                           Endpoint record on success, false otherwise
         */
        protected function validateEndpoint() {
                // The method call is invalid if we're missing parameters
                if (empty($this->module) || empty($this->action))
                        return false;

                // Validate the controller file
                $physicalControllerPath = Text::prepare(DIR_APP_MODULE_CONTROLLER, ucfirst($this->module)) .
                                                        ucfirst($this->module) . 'Controller.php';

                // Return the validation result
                return file_exists($physicalControllerPath);
        }
}