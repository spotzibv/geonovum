<?php $v = $this->meta['core']['version']; ?>
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="robots" content="noindex, nofollow">
<meta name="author" content="<?php echo BRAND_NAME; ?>">
<meta name="description" content="<?php _e('Spotzi Geonovum'); ?>">
<meta name="keywords" content="<?php _e('spotzi, geonovum, data, info, information, visualization, map, community'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Links -->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="canonical" href="<?php echo URL_BASE; ?>">
<!-- Styles -->
<link rel="stylesheet" href="<?php echo VISUALIZATION_URL_ASSET; ?>stylesheets/embed.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/jquery.chosen.min.css?v=<?php echo $v; ?>">
<link rel="stylesheet" href="/css/style.min.css?v=<?php echo $v; ?>">
<!--[if lte IE 8]>
        <link rel="stylesheet" href="/css/style.ie.min.css?v=<?php echo $v; ?>">
<![endif]-->
<!-- Scripts -->
<!-- embed.js loads jQuery 1.7.2 and jQuery UI 1.8.23 -->
<script src="<?php echo VISUALIZATION_URL_ASSET; ?>javascripts/embed.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/defaults.min.js?v=<?php echo $v; ?>"></script>
<script src="/js/modernizr-custom.min.js?v=<?php echo $v; ?>"></script>
<script src="/js/jquery.nicescroll.min.js?v=<?php echo $v; ?>"></script>
<script src="/js/jquery.chosen.min.js?v=<?php echo $v; ?>"></script>
<script src="/js/remodal.min.js?v=<?php echo $v; ?>"></script>
<script src="/js/moment.min.js?v=<?php echo $v; ?>" async></script>
<!--[if lte IE 8]>
        <script src="/js/selectivizr.min.js?v=<?php echo $v; ?>"></script>
<![endif]-->